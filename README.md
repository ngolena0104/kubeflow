
<!-- PROJECT LOGO -->

<div align="center">
  <a>
    <img src="kubeflow.png" alt="Logo">
  </a>
</div>


*The Kubeflow project is dedicated to making deployments of machine learning (ML) workflows on Kubernetes simple, portable and scalable. Our goal is not to recreate other services, but to provide a straightforward way to deploy best-of-breed open-source systems for ML to diverse infrastructures.* 


<!-- TABLE OF CONTENTS -->
<h2> Table of Contents </h2>
<p>
  <details><summary>Central Dashboard</summary></details>

  <details><summary>Notebook</summary>
  <li>
    <ul>
      <li><a href="#quickstart-guide">Quickstart Guide</a></li>
      <li><a href="#container-images">Container Images</a></li>
    </ul>
  </li>
  </details>

  <details><summary>Pipelines</summary>
  <li>
    <ul>
      <li><a href="#overview">Overview</a>
        <ul>
          <li><a href="#pipelines-interfaces">Pipelines Interfaces</li>
          <li><a href="#quickstart">Quickstart</a></li>
        </ul>
      </li>
      <li><a href="#concepts">Concepts</a></li>
      <li><a href="#pipelines-sdk">Pipelines SDK</a>
        <ul>
          <li><a href="#introduction-to-the-pipelines-sdk">Introduction to the Pipelines SDK</li>
          <li><a href="#build-a-pipeline">Build a Pipeline</a></li>
          <li><a href="#build-components">Build Components</a></li>
          <li><a href="#build-python-function-based-components">Build Python function-based components</a></li>
          <li><a href="#visualize-results-in-the-pipelines-ui">Visualize Results in the Pipelines UI</a></li>
          <li><a href="#pipeline-metrics">Pipeline Metrics</a></li>
          <li><a href="#dsl-static-type-checking">DSL Static Type Checking</a></li>
        </ul>
      </li>
    </ul>
  <li>
  </details>
    
  <details><summary>Katib</summary>
  <li>
    <ul>
        <li><a href="#introduction-to-katib">Introduction to Katib</a></li>
        <li><a href="#getting-started-with-katib">Getting started with Katib</a>
          <ul>
            <li><a href="#accessing-the-katib-ui">Accessing the Katib UI</li>
            <li><a href="#examples">Examples</a></li>
          </ul>
        </li>
        <li><a href="#running-an-experiment">Running an Experiment</a>
        <ul>
            <li><a href="#packaging-your-training-code-in-a-container-image">Packaging your training code in a container image</li>
            <li><a href="#configuring-the-experiment">Configuring the experiment</a></li>
            <li><a href="#running-the-experiment">Running the experiment</a></li>
          </ul>
        </li>
        <li><a href="#resuming-an-experiment">Resuming an Experiment</a></li>
        <li><a href="#overview-of-trial-templates">Overview of Trial Templates</a></li>
        <li><a href="#using-early-stopping">Using Early Stopping</a></li>
    </ul>
  <li>
  </details>
</p>

<!-- Central Dashboard -->

# Central Dashboard
The Kubeflow UIs include the following:

- **Home**: Home, the central hub to access recent resources, active experiments, and useful documentation.
- **Notebook Servers**: To manage Notebook servers.
- **TensorBoards**: To manage TensorBoard servers.
- **Models**: To manage deployed KFServing models.
- **Volumes**: To manage the cluster’s Volumes.
- **Experiments (AutoML)**: To manage Katib experiments.
- **Experiments (KFP)**: To manage Kubeflow Pipelines (KFP) experiments.
- **Pipelines**: To manage KFP pipelines.
- **Runs**: To manage KFP runs.
- **Recurring Runs**: To manage KFP recurring runs.
- **Artifacts**: To track ML Metadata (MLMD) artifacts.
- **Executions**: To track various component executions in MLMD.
- **Manage Contributors**: To configure user access sharing across namespaces in the Kubeflow deployment.
The central dashboard looks like this:
![This is an image](https://www.kubeflow.org/docs/images/central-ui.png)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Notebook -->

# Notebook
*Kubeflow Notebooks provides a way to run web-based development environments inside your Kubernetes cluster by running them inside Pods.*

## Quickstart Guide
Detailed steps:
1. Open the Kubeflow Central Dashboard in your browser.
2. Select a Namespace:
- Click the namespace dropdown to see the list of available namespaces.
- Choose the namespace that corresponds to your Kubeflow Profile. 
![This is an image](https://www.kubeflow.org/docs/images/notebooks-namespace.png)
3. Click “Notebook Servers” in the left-hand panel:
![This is an image](https://www.kubeflow.org/docs/images/jupyterlink.png)
4. Click “New Server” on the “Notebook Servers” page:
![This is an image](https://www.kubeflow.org/docs/images/add-notebook-server.png)
5. Enter a “Name” for your notebook server.

- The name can include letters and numbers, but no spaces.
For example, my-first-notebook.
![This is an image](https://www.kubeflow.org/docs/images/new-notebook-server.png)
6. Select a Docker “Image” for your notebook server
Custom image: If you select the custom option, you must specify a Docker image in the form registry/image:tag. 
Standard image: Click the “Image” dropdown menu to see the list of available images. (You can choose from the list configured by your Kubeflow administrator)

7. Specify the amount of “CPU” that your notebook server will request.

8. Specify the amount of “RAM” that your notebook server will request.

9. Specify a “workspace volume” to be mounted as a PVC Volume on your home folder.

10. (Optional) Specify one or more “data volumes” to be mounted as a PVC Volumes.

11. (Optional) Specify one or more additional “configurations”

These correspond to PodDefault resources which exit in your profile namespace.
Kubeflow matches the labels in the “configurations” field against the properties specified in the PodDefault manifest.
For example, select the label add-gcp-secret in the “configurations” field to match to a PodDefault manifest containing the following configuration:

```
apiVersion: kubeflow.org/v1alpha1
kind: PodDefault
metadata:
  name: add-gcp-secret
  namespace: MY_PROFILE_NAMESPACE
spec:
 selector:
  matchLabels:
    add-gcp-secret: "true"
 desc: "add gcp credential"
 volumeMounts:
 - name: secret-volume
   mountPath: /secret/gcp
 volumes:
 - name: secret-volume
   secret:
    secretName: gcp-secret
```

12. (Optional) Specify any “GPUs” that your notebook server will request.

- Kubeflow uses “limits” in Pod requests to provision GPUs onto the notebook Pods (Details about scheduling GPUs can be found in the Kubernetes Documentation.)
13. (Optional) Specify the setting for “enable shared memory”.

- Some libraries like PyTorch use shared memory for multiprocessing.
- Currently, there is no implementation in Kubernetes to activate shared memory.
- As a workaround, Kubeflow mounts an empty directory volume at /dev/shm.
14. Click “LAUNCH” to create a new Notebook CRD with your specified settings.

- You should see an entry for your new notebook server on the “Notebook Servers” page
- There should be a spinning indicator in the “Status” column.
- It can take a few minutes for kubernetes to provision the notebook server pod.
- You can check the status of your Pod by hovering your mouse cursor over the icon in the “Status” column.
15. Click “CONNECT” to view the web interface exposed by your notebook server.

![This is an image](https://v0-5.kubeflow.org/docs/images/notebook-servers.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Container Images

*Kubeflow Notebooks natively supports three types of notebooks, JupyterLab, RStudio, and Visual Studio Code (code-server), but any web-based IDE should work.*

**Base Images**

These images provide a common starting point for Kubeflow Notebook containers. See custom images to learn how to extend them with your own packages.

| Dockerfile | Registry | Notes |
| ------------- | ------------- | ------------- |
| base | public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/base:{TAG} | common base image |
| codeserver| public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/codeserver:{TAG} | base code-server (Visual Studio Code) image|
| jupyter | public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter:{TAG} | base JupyterLab image |
| rstudio  | public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/rstudio:{TAG} |	base RStudio image |


**Full Images**

| Dockerfile | Registry | Notes |
| ------------- | ------------- | ------------- |
| codeserver-python | public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/codeserver-python:{TAG}	| code-server (Visual Studio Code) + Conda Python |
| jupyter-pytorch (CPU) |	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-pytorch:{TAG} | JupyterLab + PyTorch (CPU) |
| jupyter-pytorch (CUDA)	| public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-pytorch-cuda:{TAG}	| JupyterLab + PyTorch (CUDA) |
| jupyter-pytorch-full (CPU)	| public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-pytorch-full:{TAG} |	JupyterLab + PyTorch (CPU) + common packages |
| jupyter-pytorch-full (CUDA)	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-pytorch-cuda-full:{TAG}	|	JupyterLab + PyTorch (CUDA) + common packages	|
|jupyter-scipy	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-scipy:{TAG}	|	JupyterLab + SciPy packages	|
|jupyter-tensorflow (CPU)	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-tensorflow:{TAG}	|	JupyterLab + TensorFlow (CPU)	|
|jupyter-tensorflow (CUDA)	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-tensorflow-cuda:{TAG}	|	JupyterLab + TensorFlow (CUDA)	|
|jupyter-tensorflow-full (CPU)	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-tensorflow-full:{TAG}	|	JupyterLab + TensorFlow (CPU) + common packages	|
|jupyter-tensorflow-full (CUDA)	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/jupyter-tensorflow-cuda-full:{TAG}		|JupyterLab + TensorFlow (CUDA) + common packages	|
|rstudio-tidyverse	|	public.ecr.aws/j1r0q0g6/notebooks/notebook-servers/rstudio-tidyverse:{TAG}	|	RStudio + Tidyverse packages	|

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Pipelines-->

# Pipelines

*Kubeflow Pipelines is a comprehensive solution for deploying and managing end-to-end ML workflows. Use Kubeflow Pipelines for rapid and reliable experimentation. You can schedule and compare runs, and examine detailed reports on each run.*

## Overview

### Pipelines Interfaces
*The ways you can interact with the Kubeflow Pipelines system*

- **User interface (UI)**
You can access the Kubeflow Pipelines UI by clicking Pipeline Dashboard on the Kubeflow UI. The Kubeflow Pipelines UI looks like this:
![This is an image](https://www.kubeflow.org/docs/images/pipelines/pipelines-ui.png)
From the Kubeflow Pipelines UI you can perform the following tasks:

  - Run one or more of the preloaded samples to try out pipelines quickly.
  - Upload a pipeline as a compressed file. The pipeline can be one that you have built (see how to build a pipeline) or one that someone has shared with you.
  - Create an experiment to group one or more of your pipeline runs. See the definition of an experiment.
  - Create and start a run within the experiment. A run is a single execution of a pipeline. See the definition of a run.
  - Explore the configuration, graph, and output of your pipeline run.
  - Compare the results of one or more runs within an experiment.
  - Schedule runs by creating a recurring run.

- **Python SDK**
The Kubeflow Pipelines SDK provides a set of Python packages that you can use to specify and run your ML workflows.

### Quickstart
- **Run a pipeline**
1. Click the name of the sample, [Tutorial] Data passing in python components, on the pipelines UI:
![This is an image](https://www.kubeflow.org/docs/images/click-pipeline-sample.png)
2. Click Create experiment:
![This is an image](https://www.kubeflow.org/docs/images/pipelines-start-experiment.png)
3. Follow the prompts to create an experiment and then create a run. The sample supplies default values for all the parameters you need. The following screenshot assumes you’ve already created an experiment named My experiment and are now creating a run named My first run:
![This is an image](https://www.kubeflow.org/docs/images/pipelines-start-run.png)
4. Click Start to run the pipeline.
5. Click the name of the run on the experiments dashboard:
![This is an image](https://www.kubeflow.org/docs/images/pipelines-experiments-dashboard.png)
6. Explore the graph and other aspects of your run by clicking on the components of the graph and the other UI elements:
![This is an image](https://www.kubeflow.org/docs/images/pipelines-basic-run.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Concepts
*Concepts used in Kubeflow Pipelines*

**Pipeline**

- A pipeline is a description of a machine learning (ML) workflow, including all of the components in the workflow and how the components relate to each other in the form of a graph. The pipeline configuration includes the definition of the inputs (parameters) required to run the pipeline and the inputs and outputs of each component.

**Component**

- A pipeline component is self-contained set of code that performs one step in the ML workflow (pipeline), such as data preprocessing, data transformation, model training, and so on. A component is analogous to a function, in that it has a name, parameters, return values, and a body.

**Graph**

A graph is a pictorial representation in the Kubeflow Pipelines UI of the runtime execution of a pipeline. The graph shows the steps that a pipeline run has executed or is executing, with arrows indicating the parent/child relationships between the pipeline components represented by each step. The graph is viewable as soon as the run begins. Each node within the graph corresponds to a step within the pipeline and is labeled accordingly.
![This is an image](https://gitlab.com/ngolena0104/kubeflow)

**Experiment**

An experiment is a workspace where you can try different configurations of your pipelines. You can use experiments to organize your runs into logical groups. Experiments can contain arbitrary runs, including recurring runs.

**Run and Recurring Run**

- A run is a single execution of a pipeline. Runs comprise an immutable log of all experiments that you attempt, and are designed to be self-contained to allow for reproducibility. You can track the progress of a run by looking at its details page on the Kubeflow Pipelines UI, where you can see the runtime graph, output artifacts, and logs for each step in the run.

- A recurring run, or job in the Kubeflow Pipelines backend APIs, is a repeatable run of a pipeline. The configuration for a recurring run includes a copy of a pipeline with all parameter values specified and a run trigger. You can start a recurring run inside any experiment, and it will periodically start a new copy of the run configuration. You can enable/disable the recurring run from the Kubeflow Pipelines UI. You can also specify the maximum number of concurrent runs, to limit the number of runs launched in parallel. This can be helpful if the pipeline is expected to run for a long period of time and is triggered to run frequently.

**Run triggers**

- A run trigger is a flag that tells the system when a recurring run configuration spawns a new run. The following types of run trigger are available:

  - Periodic: for an interval-based scheduling of runs (for example: every 2 hours or every 45 minutes).
  - Cron: for specifying cron semantics for scheduling runs.

**Step** 
- A step is an execution of one of the components in the pipeline. The relationship between a step and its component is one of instantiation, much like the relationship between a run and its pipeline. In a complex pipeline, components can execute multiple times in loops, or conditionally after resolving an if/else like clause in the pipeline code.


**Output Artifact**

- An output artifact is an output emitted by a pipeline component, which the Kubeflow Pipelines UI understands and can render as rich visualizations. It’s useful for pipeline components to include artifacts so that you can provide for performance evaluation, quick decision making for the run, or comparison across different runs. Artifacts also make it possible to understand how the pipeline’s various components work. An artifact can range from a plain textual view of the data to rich interactive visualizations.

**ML Metadata**

- Kubeflow Pipelines backend stores runtime information of a pipeline run in Metadata store. Runtime information includes the status of a task, availability of artifacts, custom properties associated with Execution or Artifact, etc.

<p align="right">(<a href="#top">back to top</a>)</p>

## Pipelines SDK

*The Kubeflow Pipelines SDK provides a set of Python packages that you can use to specify and run your machine learning (ML) workflows. A pipeline is a description of an ML workflow, including all of the components that make up the steps in the workflow and how the components interact with each other.*


### Introduction to the Pipelines SDK

**SDK packages:**


The Kubeflow Pipelines SDK includes the following packages:

- kfp.compiler includes classes and methods for compiling pipeline Python DSL into a workflow yaml spec Methods in this package include, but are not limited to, the following:
  - kfp.compiler.Compiler.compile compiles your Python DSL code into a single static configuration (in YAML format) that the
  Kubeflow Pipelines service can process. The Kubeflow Pipelines service converts the static configuration into a set of
  Kubernetes resources for execution.
- kfp.components includes classes and methods for interacting with pipeline components. Methods in this package include, but are not limited to, the following:
  - kfp.components.func_to_container_op converts a Python function to a pipeline component and returns a factory function. You
  can then call the factory function to construct an instance of a pipeline task (ContainerOp) that runs the original function in
  a container.

  - kfp.components.load_component_from_file loads a pipeline component from a file and returns a factory function. You can then
 call the factory function to construct an instance of a pipeline task (ContainerOp) that runs the component container image.

  - kfp.components.load_component_from_url loads a pipeline component from a URL and returns a factory function. You can then
  call the factory function to construct an instance of a pipeline task (ContainerOp) that runs the component container image.

- kfp.dsl contains the domain-specific language (DSL) that you can use to define and interact with pipelines and components.  Methods, classes, and modules in this package include, but are not limited to, the following:

  - kfp.dsl.PipelineParam represents a pipeline parameter that you can pass from one pipeline component to another. See the guide
  to pipeline parameters.

  - kfp.dsl.component is a decorator for DSL functions that returns a pipeline component. (ContainerOp).

  - kfp.dsl.pipeline is a decorator for Python functions that returns a pipeline.

  - kfp.dsl.python_component is a decorator for Python functions that adds pipeline component metadata to the function object.

  - kfp.dsl.types contains a list of types defined by the Kubeflow Pipelines SDK. Types include basic types like String, Integer,
  Float, and Bool, as well as domain-specific types like GCPProjectID and GCRPath. See the guide to DSL static type checking.

  - kfp.dsl.ResourceOp represents a pipeline task (op) which lets you directly manipulate Kubernetes resources (create, get,
  apply, …).

  - kfp.dsl.VolumeOp represents a pipeline task (op) which creates a new PersistentVolumeClaim (PVC). It aims to make the common
  case of creating a PersistentVolumeClaim fast.

  - kfp.dsl.VolumeSnapshotOp represents a pipeline task (op) which creates a new VolumeSnapshot. It aims to make the common case
  of creating a VolumeSnapshot fast.

  - kfp.dsl.PipelineVolume represents a volume used to pass data between pipeline steps. ContainerOps can mount a PipelineVolume
  either via the constructor’s argument pvolumes or add_pvolumes() method.

  - kfp.dsl.ParallelFor represents a parallel for loop over a static or dynamic set of items in a pipeline. Each iteration of the
  for loop is executed in parallel.

  - kfp.dsl.ExitHandler represents an exit handler that is invoked upon exiting a pipeline. A typical usage of ExitHandler is
  garbage collection.

  - kfp.dsl.Condition represents a group of ops, that will only be executed when a certain condition is met. The condition
  specified need to be determined at runtime, by incorporating at least one task output, or PipelineParam in the boolean
  expression.

- kfp.Client contains the Python client libraries for the Kubeflow Pipelines API. Methods in this package include, but are not limited to, the following:

  - kfp.Client.create_experiment creates a pipeline experiment and returns an experiment object.
  - kfp.Client.run_pipeline runs a pipeline and returns a run object.
  - kfp.Client.create_run_from_pipeline_func compiles a pipeline function and submits it for execution on Kubeflow Pipelines.
  - kfp.Client.create_run_from_pipeline_package runs a local pipeline package on Kubeflow Pipelines.
  - kfp.Client.upload_pipeline uploads a local file to create a new pipeline in Kubeflow Pipelines.
  - kfp.Client.upload_pipeline_version uploads a local file to create a pipeline version. Follow an example to learn more about
  creating a pipeline version

- Kubeflow Pipelines extension modules include classes and functions for specific platforms on which you can use Kubeflow Pipelines. Examples include utility functions for on premises, Google Cloud Platform (GCP), Amazon Web Services (AWS), and Microsoft Azure.

- Kubeflow Pipelines diagnose_me modulesinclude classes and functions that help with environment diagnostic tasks.

  - kfp.cli.diagnose_me.dev_env reports on diagnostic metadata from your development environment, such as your python library version.
  - kfp.cli.diagnose_me.kubernetes_cluster reports on diagnostic data from your Kubernetes cluster, such as Kubernetes secrets.
  - kfp.cli.diagnose_me.gcp reports on diagnostic data related to your GCP environment.

**Building pipelines and components**

This section summarizes the ways you can use the SDK to build pipelines and components.

A Kubeflow pipeline is a portable and scalable definition of an ML workflow. Each step in your ML workflow, such as preparing data or training a model, is an instance of a pipeline component.

A pipeline component is a self-contained set of code that performs one step in your ML workflow. Components are defined in a component specification, which defines the following:

- The component’s interface, its inputs and outputs.
- The component’s implementation, the container image and the command to execute.
- The component’s metadata, such as the name and description of the component.

Use the following options to create or reuse pipeline components.
- You can build components by defining a component specification for a containerized application.

- Lightweight Python function-based components make it easier to build a component by using the Kubeflow Pipelines SDK to generate the component specification for a Python function.

- You can reuse prebuilt components in your pipeline.

<p align="right">(<a href="#top">back to top</a>)</p>

### Build a Pipeline
*A tutorial on building pipelines to orchestrate your ML workflow*

#### Before you begin
1. Run the following command to install the Kubeflow Pipelines SDK. If you run this command in a Jupyter notebook, restart the kernel after installing the SDK.
```
$ pip install kfp --upgrade
```
2. Import the kfp and kfp.components packages.
```
import kfp
import kfp.components as comp
```
#### Understanding pipelines

A Kubeflow pipeline is a portable and scalable definition of an ML workflow, based on containers. A pipeline is composed of a set of input parameters and a list of the steps in this workflow. Each step in a pipeline is an instance of a component, which is represented as an instance of ContainerOp.

You can use pipelines to:

- Orchestrate repeatable ML workflows.
- Accelerate experimentation by running a workflow with different sets of hyperparameters.

**Understanding pipeline components:**

A pipeline component is a containerized application that performs one step in a pipeline’s workflow. Pipeline components are defined in component specifications, which define the following:

- The component’s interface, its inputs and outputs.
- The component’s implementation, the container image and the command to execute.
- The component’s metadata, such as the name and description of the component.
You can build components by defining a component specification for a containerized application, or you can use the Kubeflow Pipelines SDK to generate a component specification for a Python function. You can also reuse prebuilt components in your pipeline.

**Understanding the pipeline graph:**

Each step in your pipeline’s workflow is an instance of a component. When you define your pipeline, you specify the source of each step’s inputs. Step inputs can be set from the pipeline’s input arguments, constants, or step inputs can depend on the outputs of other steps in this pipeline. Kubeflow Pipelines uses these dependencies to define your pipeline’s workflow as a graph.

For example, consider a pipeline with the following steps: ingest data, generate statistics, preprocess data, and train a model. The following describes the data dependencies between each step.

- Ingest data: This step loads data from an external source which is specified using a pipeline argument, and it outputs a dataset. Since this step does not depend on the output of any other steps, this step can run first.
- Generate statistics: This step uses the ingested dataset to generate and output a set of statistics. Since this step depends on the dataset produced by the ingest data step, it must run after the ingest data step.
- Preprocess data: This step preprocesses the ingested dataset and transforms the data into a preprocessed dataset. Since this step depends on the dataset produced by the ingest data step, it must run after the ingest data step.
- Train a model: This step trains a model using the preprocessed dataset, the generated statistics, and pipeline parameters, such as the learning rate. Since this step depends on the preprocessed data and the generated statistics, it must run after both the preprocess data and generate statistics steps are complete.
Since the generate statistics and preprocess data steps both depend on the ingested data, the generate statistics and preprocess data steps can run in parallel. All other steps are executed once their data dependencies are available.

**Designing your pipeline:**

When designing your pipeline, think about how to split your ML workflow into pipeline components. The process of splitting an ML workflow into pipeline components is similar to the process of splitting a monolithic script into testable functions. The following rules can help you define the components that you need to build your pipeline.

- Components should have a single responsibility. Having a single responsibility makes it easier to test and reuse a component. For example, if you have a component that loads data you can reuse that for similar tasks that load data. If you have a component that loads and transforms a dataset, the component can be less useful since you can use it only when you need to load and transform that dataset.

- Reuse components when possible. Kubeflow Pipelines provides components for common pipeline tasks and for access to cloud services.

- Consider what you need to know to debug your pipeline and research the lineage of the models that your pipeline produces. Kubeflow Pipelines stores the inputs and outputs of each pipeline step. By interrogating the artifacts produced by a pipeline run, you can better understand the variations in model quality between runs or track down bugs in your workflow.

In general, you should design your components with composability in mind.

Pipelines are composed of component instances, also called steps. Steps can define their inputs as depending on the output of another step. The dependencies between steps define the pipeline workflow graph.
**Building pipeline components**

Kubeflow pipeline components are containerized applications that perform a step in your ML workflow. Here are the ways that you can define pipeline components:

- If you have a containerized application that you want to use as a pipeline component, create a component specification to define this container image as a pipeline component.

This option provides the flexibility to include code written in any language in your pipeline, so long as you can package the application as a container image. Learn more about building pipeline components.

- If your component code can be expressed as a Python function, evaluate if your component can be built as a Python function-based component. The Kubeflow Pipelines SDK makes it easier to build lightweight Python function-based components by saving you the effort of creating a component specification.

Whenever possible, reuse prebuilt components to save yourself the effort of building custom components.

The example in this guide demonstrates how to build a pipeline that uses a Python function-based component and reuses a prebuilt component.

#### Understanding how data is passed between components
When Kubeflow Pipelines runs a component, a container image is started in a Kubernetes Pod and your component’s inputs are passed in as command-line arguments. When your component has finished, the component’s outputs are returned as files.

In your component’s specification, you define the components inputs and outputs and how the inputs and output paths are passed to your program as command-line arguments. You can pass small inputs, such as short strings or numbers, to your component by value. Large inputs, such as datasets, must be passed to your component as file paths. Outputs are written to the paths that Kubeflow Pipelines provides.

Python function-based components make it easier to build pipeline components by building the component specification for you. Python function-based components also handle the complexity of passing inputs into your component and passing your function’s outputs back to your pipeline.

#### Getting started building a pipeline
The following sections demonstrate how to get started building a Kubeflow pipeline by walking through the process of converting a Python script into a pipeline.

**Design your pipeline:**
The following steps walk through some of the design decisions you may face when designing a pipeline.

1. Evaluate the process. In the following example, a Python function downloads a zipped tar file (.tar.gz) that contains several CSV files, from a public website. The function extracts the CSV files and then merges them into a single file.
```
import glob
import pandas as pd
import tarfile
import urllib.request
    
def download_and_merge_csv(url: str, output_csv: str):
  with urllib.request.urlopen(url) as res:
    tarfile.open(fileobj=res, mode="r|gz").extractall('data')
  df = pd.concat(
      [pd.read_csv(csv_file, header=None) 
       for csv_file in glob.glob('data/*.csv')])
  df.to_csv(output_csv, index=False, header=False)
```
2. Run the following Python command to test the function.
```
download_and_merge_csv(
    url='https://storage.googleapis.com/ml-pipeline-playground/iris-csv-files.tar.gz', 
    output_csv='merged_data.csv')
```
3. Run the following to print the first few rows of the merged CSV file.

```
$ head merged_data.csv
```
4. Design your pipeline. For example, consider the following pipeline designs.
- Implement the pipeline using a single step. In this case, the pipeline contains one component that works similarly to the example function. This is a straightforward function, and implementing a single-step pipeline is a reasonable approach in this case.

- The down side of this approach is that the zipped tar file would not be an artifact of your pipeline runs. Not having this artifact available could make it harder to debug this component in production.

- Implement this as a two-step pipeline. The first step downloads a file from a website. The second step extracts the CSV files from a zipped tar file and merges them into a single file.

This approach has a few benefits:

  - You can reuse the Web Download component to implement the first step.
  - Each step has a single responsibility, which makes the components easier to reuse.
  - The zipped tar file is an artifact of the first pipeline step. This means that you can examine this artifact when debugging
  pipelines that use this component.
This example implements a two-step pipeline.

**Build your pipeline components:**

1. Build your pipeline components. This example modifies the initial script to extract the contents of a zipped tar file, merge the CSV files that were contained in the zipped tar file, and return the merged CSV file.

This example builds a Python function-based component. You can also package your component’s code as a Docker container image and define the component using a ComponentSpec.

In this case, the following modifications were required to the original function.

- The file download logic was removed. The path to the zipped tar file is passed as an argument to this function.
- The import statements were moved inside of the function. Python function-based components require standalone Python functions.This means that any required import statements must be defined within the function, and any helper functions must be defined within the function. Learn more about building Python function-based components.
- The function’s arguments are decorated with the kfp.components.InputPath and the kfp.components.OutputPath annotations. These annotations let Kubeflow Pipelines know to provide the path to the zipped tar file and to create a path where your function stores the merged CSV file.
The following example shows the updated merge_csv function.
```
def merge_csv(file_path: comp.InputPath('Tarball'),
              output_csv: comp.OutputPath('CSV')):
  import glob
  import pandas as pd
  import tarfile

  tarfile.open(name=file_path, mode="r|gz").extractall('data')
  df = pd.concat(
      [pd.read_csv(csv_file, header=None) 
       for csv_file in glob.glob('data/*.csv')])
  df.to_csv(output_csv, index=False, header=False)
```
2. Use kfp.components.create_component_from_func to return a factory function that you can use to create pipeline steps. This example also specifies the base container image to run this function in, the path to save the component specification to, and a list of PyPI packages that need to be installed in the container at runtime.
```
create_step_merge_csv = kfp.components.create_component_from_func(
    func=merge_csv,
    output_component_file='component.yaml', # This is optional. It saves the component spec for future use.
    base_image='python:3.7',
    packages_to_install=['pandas==1.1.4'])
```
**Build your pipeline:**

1. Use kfp.components.load_component_from_url to load the component specification YAML for any components that you are reusing in this pipeline.
```
web_downloader_op = kfp.components.load_component_from_url(
    'https://raw.githubusercontent.com/kubeflow/pipelines/master/components/web/Download/component.yaml')
```
2. Define your pipeline as a Python function.
Your pipeline function’s arguments define your pipeline’s parameters. Use pipeline parameters to experiment with different hyperparameters, such as the learning rate used to train a model, or pass run-level inputs, such as the path to an input file, into a pipeline run.

Use the factory functions created by kfp.components.create_component_from_func and kfp.components.load_component_from_url to create your pipeline’s tasks. The inputs to the component factory functions can be pipeline parameters, the outputs of other tasks, or a constant value. In this case, the web_downloader_task task uses the url pipeline parameter, and the merge_csv_task uses the data output of the web_downloader_task.
```
# Define a pipeline and create a task from a component:
def my_pipeline(url):
  web_downloader_task = web_downloader_op(url=url)
  merge_csv_task = create_step_merge_csv(file=web_downloader_task.outputs['data'])
  # The outputs of the merge_csv_task can be referenced using the
  # merge_csv_task.outputs dictionary: merge_csv_task.outputs['output_csv']
```
**Compile and run your pipeline:**

After defining the pipeline in Python as described in the preceding section, use one of the following options to compile the pipeline and submit it to the Kubeflow Pipelines service.

Option 1: Compile and then upload in UI 
1. Run the following to compile your pipeline and save it as pipeline.yaml.
```
kfp.compiler.Compiler().compile(
    pipeline_func=my_pipeline,
    package_path='pipeline.yaml')
```
2. Upload and run your pipeline.yaml using the Kubeflow Pipelines user interface. 
Option 2: run the pipeline using Kubeflow Pipelines SDK client 
1. Create an instance of the kfp.Client class following steps in connecting to Kubeflow Pipelines using the SDK client.
```
client = kfp.Client() # change arguments accordingly
```
2. Run the pipeline using the kfp.Client instance:
```
client.create_run_from_pipeline_func(
    my_pipeline,
    arguments={
        'url': 'https://storage.googleapis.com/ml-pipeline-playground/iris-csv-files.tar.gz'
    })
```
<p align="right">(<a href="#top">back to top</a>)</p>

### Build Components
*A tutorial on how to create components and use them in a pipeline*

#### Before you begin 

Run the following command to install the Kubeflow Pipelines SDK. 
```
$ pip3 install kfp --upgrade
```
#### Understanding pipeline components
Pipeline components are self-contained sets of code that perform one step in your ML workflow, such as preprocessing data or training a model. To create a component, you must build the component’s implementation and define the component specification.

Your component’s implementation includes the component’s executable code and the Docker container image that the code runs in. 
Once you have built your component’s implementation, you can define your component’s interface as a component specification. A component specification defines:

- The component’s inputs and outputs.
- The container image that your component’s code runs in, the command to use to run your component’s code, and the command-line arguments to pass to your component’s code.
- The component’s metadata, such as the name and description.

If your component’s code is implemented as a Python function, use the Kubeflow Pipelines SDK to package your function as a component.
#### Designing a pipeline component 
When Kubeflow Pipelines executes a component, a container image is started in a Kubernetes Pod and your component’s inputs are passed in as command-line arguments. You can pass small inputs, such as strings and numbers, by value. Larger inputs, such as CSV data, must be passed as paths to files. When your component has finished, the component’s outputs are returned as files.

When you design your component’s code, consider the following:

- Which inputs can be passed to your component by value? Examples of inputs that you can pass by value include numbers, booleans, and short strings. Any value that you could reasonably pass as a command-line argument can be passed to your component by value. All other inputs are passed to your component by a reference to the input’s path.
- To return an output from your component, the output’s data must be stored as a file. When you define your component, you let Kubeflow Pipelines know what outputs your component produces. When your pipeline runs, Kubeflow Pipelines passes the paths that you use to store your component’s outputs as inputs to your component.
- Outputs are typically written to a single file. In some cases, you may need to return a directory of files as an output. In this case, create a directory at the output path and write the output files to that location. In both cases, it may be necessary to create parent directories if they do not exist.
- Your component’s goal may be to create a dataset in an external service, such as a BigQuery table. In this case, it may make sense for the component to output an identifier for the produced data, such as a table name, instead of the data itself. We recommend that you limit this pattern to cases where the data must be put into an external system instead of keeping it inside the Kubeflow Pipelines system.
- Since your inputs and output paths are passed in as command-line arguments, your component’s code must be able to read inputs from the command line. If your component is built with Python, libraries such as argparse and absl.flags make it easier to read your component’s inputs.
- Your component’s code can be implemented in any language, so long as it can run in a container image.
The following is an example program written using Python3. This program reads a given number of lines from an input file and writes those lines to an output file. This means that this function accepts three command-line parameters:

- The path to the input file.
- The number of lines to read.
- The path to the output file.
```
#!/usr/bin/env python3
import argparse
from pathlib import Path

# Function doing the actual work (Outputs first N lines from a text file)
def do_work(input1_file, output1_file, param1):
  for x, line in enumerate(input1_file):
    if x >= param1:
      break
    _ = output1_file.write(line)
  
# Defining and parsing the command-line arguments
parser = argparse.ArgumentParser(description='My program description')
# Paths must be passed in, not hardcoded
parser.add_argument('--input1-path', type=str,
  help='Path of the local file containing the Input 1 data.')
parser.add_argument('--output1-path', type=str,
  help='Path of the local file where the Output 1 data should be written.')
parser.add_argument('--param1', type=int, default=100,
  help='The number of lines to read from the input and write to the output.')
args = parser.parse_args()

# Creating the directory where the output file is created (the directory
# may or may not exist).
Path(args.output1_path).parent.mkdir(parents=True, exist_ok=True)

with open(args.input1_path, 'r') as input1_file:
    with open(args.output1_path, 'w') as output1_file:
        do_work(input1_file, output1_file, args.param1)

```
If this program is saved as program.py, the command-line invocation of this program is:

```
python3 program.py --input1-path <path-to-the-input-file> \
  --param1 <number-of-lines-to-read> \
  --output1-path <path-to-write-the-output-to> 

```

#### Containerize your component’s code 

For Kubeflow Pipelines to run your component, your component must be packaged as a Docker container image and published to a container registry that your Kubernetes cluster can access. The steps to create a container image are not specific to Kubeflow Pipelines. To make things easier for you, this section provides some guidelines on standard container creation.

1.Create a Dockerfile for your container. A Dockerfile specifies:

  - The base container image. For example, the operating system that your code runs on.
  - Any dependencies that need to be installed for your code to run.
  - Files to copy into the container, such as the runnable code for this component.
  The following is an example Dockerfile.
  ```
  FROM python:3.7
  RUN python3 -m pip install keras
  COPY ./src /pipelines/component/src
  ```
  In this example:

  - The base container image is python:3.7.
  - The keras Python package is installed in the container image.
  - Files in your ./src directory are copied into /pipelines/component/src in the container image.
2. Create a script named build_image.sh that uses Docker to build your container image and push your container image to a container registry. Your Kubernetes cluster must be able to access your container registry to run your component. Examples of container registries include Google Container Registry and Docker Hub.

  The following example builds a container image, pushes it to a container registry, and outputs the strict image name. It is a best practice to use the strict image name in your component specification to ensure that you are using the expected version of a container image in each component execution.
  ```
  #!/bin/bash -e
  image_name=gcr.io/my-org/my-image
  image_tag=latest
  full_image_name=${image_name}:${image_tag}

  cd "$(dirname "$0")" 
  docker build -t "${full_image_name}" .
  docker push "$full_image_name"

  # Output the strict image name, which contains the sha256 image digest
  docker inspect --format="{{index .RepoDigests 0}}" "${full_image_name}"
  ```
  In the preceding example:

  - The image_name specifies the full name of your container image in the container registry.
  - The image_tag specifies that this image should be tagged as latest.
  Save this file and run the following to make this script executable.
  ```
  chmod +x build_image.sh

  ```

3. Run your build_image.sh script to build your container image and push it to a container registry.

4. Use docker run to test your container image locally. If necessary, revise your application and Dockerfile until your application works as expected in the container.


#### Creating a component specification 
To create a component from your containerized program, you must create a component specification that defines the component’s interface and implementation. The following sections provide an overview of how to create a component specification by demonstrating how to define the component’s implementation, interface, and metadata.

**Define your component’s implementation**

The following example creates a component specification YAML and defines the component’s implementation.

1. Create a file named component.yaml and open it in a text editor.

2. Create your component’s implementation section and specify the strict name of your container image. The strict image name is provided when you run your build_image.sh script.
  ```
  implementation:
    container:
      # The strict name of a container image that you've pushed to a container registry.
      image: gcr.io/my-org/my-image@sha256:a172..752f
  ```
3. Define a command for your component’s implementation. This field specifies the command-line arguments that are used to run your program in the container.
  ```
  implementation:
    container:
      image: gcr.io/my-org/my-image@sha256:a172..752f
      # command is a list of strings (command-line arguments). 
      # The YAML language has two syntaxes for lists and you can use either of them. 
      # Here we use the "flow syntax" - comma-separated strings inside square brackets.
      command: [
        python3, 
        # Path of the program inside the container
        /pipelines/component/src/program.py,
        --input1-path,
        {inputPath: Input 1},
        --param1, 
        {inputValue: Parameter 1},
        --output1-path, 
        {outputPath: Output 1},
      ]
  ```
  The command is formatted as a list of strings. Each string in the command is a command-line argument or a placeholder. At runtime, placeholders are replaced with an input or output. In the preceding example, two inputs and one output path are passed into a Python script at /pipelines/component/src/program.py.

  There are three types of input/output placeholders:

  - {inputValue: <input-name>}: This placeholder is replaced with the value of the specified input. This is useful for small pieces of input data, such as numbers or small strings.

  - {inputPath: <input-name>}: This placeholder is replaced with the path to this input as a file. Your component can read the contents of that input at that path during the pipeline run.

  - {outputPath: <output-name>}: This placeholder is replaced with the path where your program writes this output’s data. This lets the Kubeflow Pipelines system read the contents of the file and store it as the value of the specified output.

  The <input-name> name must match the name of an input in the inputs section of your component specification. The <output-name> name must match the name of an output in the outputs section of your component specification.

**Define your component’s interface**

The following examples demonstrate how to specify your component’s interface.

1. To define an input in your component.yaml, add an item to the inputs list with the following attributes:

  - name: Human-readable name of this input. Each input’s name must be unique.
  - description: (Optional.) Human-readable description of the input.
  - default: (Optional.) Specifies the default value for this input.
  - type: (Optional.) Specifies the input’s type. Learn more about the types defined in the Kubeflow Pipelines SDK and how type checking works in pipelines and components.
  - optional: Specifies if this input is optional. The value of this attribute is of type Bool, and defaults to False.
  In this example, the Python program has two inputs:

  - Input 1 contains String data.
  - Parameter 1 contains an Integer.
  ```
  inputs:
  - {name: Input 1, type: String, description: 'Data for input 1'}
  - {name: Parameter 1, type: Integer, default: '100', description: 'Number of lines to copy'}
  ```
  Note: Input 1 and Parameter 1 do not specify any details about how they are stored or how much data they contain. Consider using naming conventions to indicate if inputs are expected to be small enough to pass by value.

2. After your component finishes its task, the component’s outputs are passed to your pipeline as paths. At runtime, Kubeflow Pipelines creates a path for each of your component’s outputs. These paths are passed as inputs to your component’s implementation.

  To define an output in your component specification YAML, add an item to the outputs list with the following attributes:

  - name: Human-readable name of this output. Each output’s name must be unique.
  - description: (Optional.) Human-readable description of the output.
  - type: (Optional.) Specifies the output’s type. Learn more about the types defined in the Kubeflow Pipelines SDK and how type checking works in pipelines and components.
  In this example, the Python program returns one output. The output is named Output 1 and it contains String data.
  ```
  outputs:
  - {name: Output 1, type: String, description: 'Output 1 data.'}
  ```
  Note: Consider using naming conventions to indicate if this output is expected to be small enough to pass by value. You should limit the amount of data that is passed by value to 200 KB per pipeline run.

3. After you define your component’s interface, the component.yaml should be something like the following:
```
inputs:
- {name: Input 1, type: String, description: 'Data for input 1'}
- {name: Parameter 1, type: Integer, default: '100', description: 'Number of lines to copy'}

outputs:
- {name: Output 1, type: String, description: 'Output 1 data.'}

implementation:
  container:
    image: gcr.io/my-org/my-image@sha256:a172..752f
    # command is a list of strings (command-line arguments). 
    # The YAML language has two syntaxes for lists and you can use either of them. 
    # Here we use the "flow syntax" - comma-separated strings inside square brackets.
    command: [
      python3, 
      # Path of the program inside the container
      /pipelines/component/src/program.py,
      --input1-path,
      {inputPath: Input 1},
      --param1, 
      {inputValue: Parameter 1},
      --output1-path, 
      {outputPath: Output 1},
    ]
  ```
**Specify your component’s metadata**

To define your component’s metadata, add the name and description fields to your component.yaml
```
name: Get Lines
description: Gets the specified number of lines from the input file.

inputs:
- {name: Input 1, type: String, description: 'Data for input 1'}
- {name: Parameter 1, type: Integer, default: '100', description: 'Number of lines to copy'}

outputs:
- {name: Output 1, type: String, description: 'Output 1 data.'}

implementation:
  container:
    image: gcr.io/my-org/my-image@sha256:a172..752f
    # command is a list of strings (command-line arguments). 
    # The YAML language has two syntaxes for lists and you can use either of them. 
    # Here we use the "flow syntax" - comma-separated strings inside square brackets.
    command: [
      python3, 
      # Path of the program inside the container
      /pipelines/component/src/program.py,
      --input1-path,
      {inputPath: Input 1},
      --param1, 
      {inputValue: Parameter 1},
      --output1-path, 
      {outputPath: Output 1},
    ]
  ```
#### Using your component in a pipeline
You can use the Kubeflow Pipelines SDK to load your component using methods such as the following:

- kfp.components.load_component_from_file: Use this method to load your component from a component.yaml path.
- kfp.components.load_component_from_url: Use this method to load a component.yaml from a URL.
- kfp.components.load_component_from_text: Use this method to load your component specification YAML from a string. This method is useful for rapidly iterating on your component specification.
These functions create a factory function that you can use to create ContainerOp instances to use as steps in your pipeline. This factory function’s input arguments include your component’s inputs and the paths to your component’s outputs. The function signature may be modified in the following ways to ensure that it is valid and Pythonic.

- Inputs with default values will come after the inputs without default values and outputs.
- Input and output names are converted to Pythonic names (spaces and symbols are replaced with underscores and letters are converted to lowercase). For example, an input named Input 1 is converted to input_1.
The following example demonstrates how to load the text of your component specification and run it in a two-step pipeline. Before you run this example, update the component specification to use the component specification you defined in the previous sections.

To demonstrate data passing between components, we create another component that simply uses bash commands to write some text value to an output file. And the output file can be passed to our previous component as an input.
```
import kfp
import kfp.components as comp

create_step_get_lines = comp.load_component_from_text("""
name: Get Lines
description: Gets the specified number of lines from the input file.

inputs:
- {name: Input 1, type: Data, description: 'Data for input 1'}
- {name: Parameter 1, type: Integer, default: '100', description: 'Number of lines to copy'}

outputs:
- {name: Output 1, type: Data, description: 'Output 1 data.'}

implementation:
  container:
    image: gcr.io/my-org/my-image@sha256:a172..752f
    # command is a list of strings (command-line arguments). 
    # The YAML language has two syntaxes for lists and you can use either of them. 
    # Here we use the "flow syntax" - comma-separated strings inside square brackets.
    command: [
      python3, 
      # Path of the program inside the container
      /pipelines/component/src/program.py,
      --input1-path,
      {inputPath: Input 1},
      --param1, 
      {inputValue: Parameter 1},
      --output1-path, 
      {outputPath: Output 1},
    ]""")

# create_step_get_lines is a "factory function" that accepts the arguments
# for the component's inputs and output paths and returns a pipeline step
# (ContainerOp instance).
#
# To inspect the get_lines_op function in Jupyter Notebook, enter 
# "get_lines_op(" in a cell and press Shift+Tab.
# You can also get help by entering `help(get_lines_op)`, `get_lines_op?`,
# or `get_lines_op??`.

# Create a simple component using only bash commands. The output of this component
# can be passed to a downstream component that accepts an input with the same type.
create_step_write_lines = comp.load_component_from_text("""
name: Write Lines
description: Writes text to a file.

inputs:
- {name: text, type: String}

outputs:
- {name: data, type: Data}

implementation:
  container:
    image: busybox
    command:
    - sh
    - -c
    - |
      mkdir -p "$(dirname "$1")"
      echo "$0" > "$1"
    args:
    - {inputValue: text}
    - {outputPath: data}
""")

# Define your pipeline 
def my_pipeline():
    write_lines_step = create_step_write_lines(
        text='one\ntwo\nthree\nfour\nfive\nsix\nseven\neight\nnine\nten')

    get_lines_step = create_step_get_lines(
        # Input name "Input 1" is converted to pythonic parameter name "input_1"
        input_1=write_lines_step.outputs['data'],
        parameter_1='5',
    )

# If you run this command on a Jupyter notebook running on Kubeflow,
# you can exclude the host parameter.
# client = kfp.Client()
client = kfp.Client(host='<your-kubeflow-pipelines-host-name>')

# Compile, upload, and submit this pipeline for execution.
client.create_run_from_pipeline_func(my_pipeline, arguments={})
```

#### Organizing the component files
This section provides a recommended way to organize a component’s files. There is no requirement that you must organize the files in this way. However, using the standard organization makes it possible to reuse the same scripts for testing, image building, and component versioning.
```
components/<component group>/<component name>/

    src/*            # Component source code files
    tests/*          # Unit tests
    run_tests.sh     # Small script that runs the tests
    README.md        # Documentation. If multiple files are needed, move to docs/.

    Dockerfile       # Dockerfile to build the component container image
    build_image.sh   # Small script that runs docker build and docker push

    component.yaml   # Component definition in YAML format
```
<p align="right">(<a href="#top">back to top</a>)</p>

### Build Python function-based components
*Building your own lightweight pipelines components using Python*

A Kubeflow Pipelines component is a self-contained set of code that performs one step in your ML workflow. A pipeline component is composed of:

- The component code, which implements the logic needed to perform a step in your ML workflow.

- A component specification, which defines the following:

  - The component’s metadata, its name and description.
  - The component’s interface, the component’s inputs and outputs.
  - The component’s implementation, the Docker container image to run, how to pass inputs to your component code, and how to get
  the component’s outputs.
Python function-based components make it easier to iterate quickly by letting you build your component code as a Python function and generating the component specification for you. This document describes how to build Python function-based components and use them in your pipeline

#### Before you begin 
1. Run the following command to install the Kubeflow Pipelines SDK. If you run this command in a Jupyter notebook, restart the kernel after installing the SDK.
```
$ pip3 install kfp --upgrade
```
2. Import the kfp package.
```
import kfp
from kfp.components import create_component_from_func
```
3. Create an instance of the kfp.Client class following steps in connecting to Kubeflow Pipelines using the SDK client.
```
client = kfp.Client() # change arguments accordingly
```
#### Getting started with Python function-based components
This section demonstrates how to get started building Python function-based components by walking through the process of creating a simple component.

1. Define your component’s code as a standalone python function. In this example, the function adds two floats and returns the sum of the two arguments.
```
def add(a: float, b: float) -> float:
  '''Calculates sum of two arguments'''
  return a + b
```
2. Use kfp.components.create_component_from_func to generate the component specification YAML and return a factory function that you can use to create kfp.dsl.ContainerOp class instances for your pipeline. The component specification YAML is a reusable and shareable definition of your component.
```
add_op = create_component_from_func(
    add, output_component_file='add_component.yaml')
```
3. Create and run your pipeline. 
```
import kfp.dsl as dsl
@dsl.pipeline(
  name='Addition pipeline',
  description='An example pipeline that performs addition calculations.'
)
def add_pipeline(
  a='1',
  b='7',
):
  # Passes a pipeline parameter and a constant value to the `add_op` factory
  # function.
  first_add_task = add_op(a, 4)
  # Passes an output reference from `first_add_task` and a pipeline parameter
  # to the `add_op` factory function. For operations with a single return
  # value, the output reference can be accessed as `task.output` or
  # `task.outputs['output_name']`.
  second_add_task = add_op(first_add_task.output, b)

# Specify argument values for your pipeline run.
arguments = {'a': '7', 'b': '8'}

# Create a pipeline run, using the client you initialized in a prior step.
client.create_run_from_pipeline_func(add_pipeline, arguments=arguments)
```
#### Building Python function-based components
Use the following instructions to build a Python function-based component:

1. Define a standalone Python function. This function must meet the following requirements:

- It should not use any code declared outside of the function definition.
- Import statements must be added inside the function. 
- Helper functions must be defined inside this function.
2. Kubeflow Pipelines uses your function’s inputs and outputs to define your component’s interface. Learn more about passing data between components. Your function’s inputs and outputs must meet the following requirements:

- If the function accepts or returns large amounts of data or complex data types, you must pass that data as a file.
- If the function accepts numeric values as parameters, the parameters must have type hints. Supported types are int and float. Otherwise, parameters are passed as strings.
- If your component returns multiple small outputs (short strings, numbers, or booleans), annotate your function with the typing.NamedTuple type hint and use the collections.namedtuple function return your function’s outputs as a new subclass of tuple. For an example, read Passing parameters by value.
3. (Optional.) If your function has complex dependencies, choose or build a container image for your Python function to run in. 
4. Call kfp.components.create_component_from_func(func) to convert your function into a pipeline component.

- func: The Python function to convert.
- base_image: (Optional.) Specify the Docker container image to run this function in. Learn more about selecting or building a container image.
- output_component_file: (Optional.) Writes your component definition to a file. You can use this file to share the component with colleagues or reuse it in different pipelines.
- packages_to_install: (Optional.) A list of versioned Python packages to install before running your function.

**Using and installing Python packages:**

When Kubeflow Pipelines runs your pipeline, each component runs within a Docker container image on a Kubernetes Pod. To load the packages that your Python function depends on, one of the following must be true:

- The package must be installed on the container image.
- The package must be defined using the packages_to_install parameter of the kfp.components.create_component_from_func(func) function.
- Your function must install the package. For example, your function can use the subprocess module to run a command like pip install that installs a package.

**Selecting or building a container image:**

Currently, if you do not specify a container image, your Python-function based component uses the python:3.7 container image. If your function has complex dependencies, you may benefit from using a container image that has your dependencies preinstalled, or building a custom container image. Preinstalling your dependencies reduces the amount of time that your component runs in, since your component does not need to download and install packages each time it runs.

Many frameworks, such as TensorFlow and PyTorch, and cloud service providers offer prebuilt container images that have common dependencies installed.

If a prebuilt container is not available, you can build a custom container image with your Python function’s dependencies. For more information about building a custom container, read the Dockerfile reference guide in the Docker documentation.

If you build or select a container image, instead of using the default container image, the container image must use Python 3.5 or later.

**Understanding how data is passed between components:**

When Kubeflow Pipelines runs your component, a container image is started in a Kubernetes Pod and your component’s inputs are passed in as command-line arguments. When your component has finished, the component’s outputs are returned as files.

Python function-based components make it easier to build pipeline components by building the component specification for you. Python function-based components also handle the complexity of passing inputs into your component and passing your function’s outputs back to your pipeline.

The following sections describe how to pass parameters by value and by file.

- Parameters that are passed by value include numbers, booleans, and short strings. Kubeflow Pipelines passes parameters to your component by value, by passing the values as command-line arguments.
- Parameters that are passed by file include CSV, images, and complex types. These files are stored in a location that is accessible to your component running on Kubernetes, such as a persistent volume claim or a cloud storage service. Kubeflow Pipelines passes parameters to your component by file, by passing their paths as a command-line argument.

**Input and output parameter names:**

When you use the Kubeflow Pipelines SDK to convert your Python function to a pipeline component, the Kubeflow Pipelines SDK uses the function’s interface to define the interface of your component in the following ways.

- Some arguments define input parameters.
- Some arguments define output parameters.
- The function’s return value is used as an output parameter. If the return value is a collections.namedtuple, the named tuple is used to return several small values.
Since you can pass parameters between components as a value or as a path, the Kubeflow Pipelines SDK removes common parameter suffixes that leak the component’s expected implementation. For example, a Python function-based component that ingests data and outputs CSV data may have an output argument that is defined as csv_path: comp.OutputPath(str). In this case, the output is the CSV data, not the path. So, the Kubeflow Pipelines SDK simplifies the output name to csv.

The Kubeflow Pipelines SDK uses the following rules to define the input and output parameter names in your component’s interface:

- If the argument name ends with _path and the argument is annotated as an kfp.components.InputPath or kfp.components.OutputPath, the parameter name is the argument name with the trailing _path removed.
- If the argument name ends with _file, the parameter name is the argument name with the trailing _file removed.
- If you return a single small value from your component using the return statement, the output parameter is named output.
- If you return several small values from your component by returning a collections.namedtuple, the Kubeflow Pipelines SDK uses the tuple’s field names as the output parameter names.
Otherwise, the Kubeflow Pipelines SDK uses the argument name as the parameter name.


**Passing parameters by value:**

Python function-based components make it easier to pass parameters between components by value (such as numbers, booleans, and short strings), by letting you define your component’s interface by annotating your Python function. The supported types are int, float, bool, and str. You can also pass list or dict instances by value, if they contain small values, such as int, float, bool, or str values. If you do not annotate your function, these input parameters are passed as strings.

If your component returns multiple outputs by value, annotate your function with the typing.NamedTuple type hint and use the collections.namedtuple function to return your function’s outputs as a new subclass of tuple.

You can also return metadata and metrics from your function.

- Metadata helps you visualize pipeline results. 
- Metrics help you compare pipeline runs. 
The following example demonstrates how to return multiple outputs by value, including component metadata and metrics.
```
from typing import NamedTuple
def multiple_return_values_example(a: float, b: float) -> NamedTuple(
  'ExampleOutputs',
  [
    ('sum', float),
    ('product', float),
    ('mlpipeline_ui_metadata', 'UI_metadata'),
    ('mlpipeline_metrics', 'Metrics')
  ]):
  """Example function that demonstrates how to return multiple values."""  
  sum_value = a + b
  product_value = a * b

  # Export a sample tensorboard
  metadata = {
    'outputs' : [{
      'type': 'tensorboard',
      'source': 'gs://ml-pipeline-dataset/tensorboard-train',
    }]
  }

  # Export two metrics
  metrics = {
    'metrics': [
      {
        'name': 'sum',
        'numberValue':  float(sum_value),
      },{
        'name': 'product',
        'numberValue':  float(product_value),
      }
    ]  
  }

  from collections import namedtuple
  example_output = namedtuple(
      'ExampleOutputs',
      ['sum', 'product', 'mlpipeline_ui_metadata', 'mlpipeline_metrics'])
  return example_output(sum_value, product_value, metadata, metrics)
```

**Passing parameters by file:**

Python function-based components make it easier to pass files to your component, or to return files from your component, by letting you annotate your Python function’s parameters to specify which parameters refer to a file. Your Python function’s parameters can refer to either input or output files. If your parameter is an output file, Kubeflow Pipelines passes your function a path or stream that you can use to store your output file.

The following example accepts a file as an input and returns two files as outputs.

```
def split_text_lines(
    source_path: comp.InputPath(str),
    odd_lines_path: comp.OutputPath(str),
    even_lines_path: comp.OutputPath(str)):
    """Splits a text file into two files, with even lines going to one file
    and odd lines to the other."""

    with open(source_path, 'r') as reader:
        with open(odd_lines_path, 'w') as odd_writer:
            with open(even_lines_path, 'w') as even_writer:
                while True:
                    line = reader.readline()
                    if line == "":
                        break
                    odd_writer.write(line)
                    line = reader.readline()
                    if line == "":
                        break
                    even_writer.write(line)

```

In this example, the inputs and outputs are defined as parameters of the split_text_lines function. This lets Kubeflow Pipelines pass the path to the source data file and the paths to the output data files into the function.

To accept a file as an input parameter, use one of the following type annotations:

- kfp.components.InputBinaryFile: Use this annotation to specify that your function expects a parameter to be an io.BytesIO instance that this function can read.
- kfp.components.InputPath: Use this annotation to specify that your function expects a parameter to be the path to the input file as a string.
- kfp.components.InputTextFile: Use this annotation to specify that your function expects a parameter to be an io.TextIOWrapper instance that this function can read.
To return a file as an output, use one of the following type annotations:

- kfp.components.OutputBinaryFile: Use this annotation to specify that your function expects a parameter to be an io.BytesIO instance that this function can write to.
- kfp.components.OutputPath: Use this annotation to specify that your function expects a parameter to be the path to store the output file at as a string.
- kfp.components.OutputTextFile: Use this annotation to specify that your function expects a parameter to be an io.TextIOWrapper that this function can write to.

#### Example Python function-based component

This section demonstrates how to build a Python function-based component that uses imports, helper functions, and produces multiple outputs.

Define your function. This example function uses the numpy package to calculate the quotient and remainder for a given dividend and divisor in a helper function. In addition to the quotient and remainder, the function also returns metadata for visualization and two metrics.
```
from typing import NamedTuple

def my_divmod(
  dividend: float,
  divisor: float) -> NamedTuple(
    'MyDivmodOutput',
    [
      ('quotient', float),
      ('remainder', float),
      ('mlpipeline_ui_metadata', 'UI_metadata'),
      ('mlpipeline_metrics', 'Metrics')
    ]):
    '''Divides two numbers and calculate  the quotient and remainder'''

    # Import the numpy package inside the component function
    import numpy as np

    # Define a helper function
    def divmod_helper(dividend, divisor):
        return np.divmod(dividend, divisor)

    (quotient, remainder) = divmod_helper(dividend, divisor)

    from tensorflow.python.lib.io import file_io
    import json

    # Export a sample tensorboard
    metadata = {
      'outputs' : [{
        'type': 'tensorboard',
        'source': 'gs://ml-pipeline-dataset/tensorboard-train',
      }]
    }

    # Export two metrics
    metrics = {
      'metrics': [{
          'name': 'quotient',
          'numberValue':  float(quotient),
        },{
          'name': 'remainder',
          'numberValue':  float(remainder),
        }]}

    from collections import namedtuple
    divmod_output = namedtuple('MyDivmodOutput',
        ['quotient', 'remainder', 'mlpipeline_ui_metadata',
         'mlpipeline_metrics'])
    return divmod_output(quotient, remainder, json.dumps(metadata),
                         json.dumps(metrics))
```
2. Test your function by running it directly, or with unit tests.
```
my_divmod(100, 7)
```
3. This should return a result like the following:
```
MyDivmodOutput(quotient=14, remainder=2, mlpipeline_ui_metadata='{"outputs": [{"type": "tensorboard", "source": "gs://ml-pipeline-dataset/tensorboard-train"}]}', mlpipeline_metrics='{"metrics": [{"name": "quotient", "numberValue": 14.0}, {"name": "remainder", "numberValue": 2.0}]}')
```

4. Use kfp.components.create_component_from_func to return a factory function that you can use to create kfp.dsl.ContainerOp class instances for your pipeline. This example also specifies the base container image to run this function in.

```
divmod_op = comp.create_component_from_func(
    my_divmod, base_image='tensorflow/tensorflow:1.11.0-py3')
```

5. Define your pipeline. This example uses the divmod_op factory function and the add_op factory function from an earlier example.

```
import kfp.dsl as dsl
@dsl.pipeline(
   name='Calculation pipeline',
   description='An example pipeline that performs arithmetic calculations.'
)
def calc_pipeline(
   a='1',
   b='7',
   c='17',
):
    # Passes a pipeline parameter and a constant value as operation arguments.
    add_task = add_op(a, 4) # The add_op factory function returns
                            # a dsl.ContainerOp class instance. 

    # Passes the output of the add_task and a pipeline parameter as operation
    # arguments. For an operation with a single return value, the output
    # reference is accessed using `task.output` or
    # `task.outputs['output_name']`.
    divmod_task = divmod_op(add_task.output, b)

    # For an operation with multiple return values, output references are
    # accessed as `task.outputs['output_name']`.
    result_task = add_op(divmod_task.outputs['quotient'], c)
```

6. Compile and run your pipeline.

```
# Specify pipeline argument values
arguments = {'a': '7', 'b': '8'}

# Submit a pipeline run
client.create_run_from_pipeline_func(calc_pipeline, arguments=arguments)
```

###### Pipeline Parameters

The kfp.dsl.PipelineParam class represents a reference to future data that will be passed to the pipeline or produced by a task.

Your pipeline function should have parameters, so that they can later be configured in the Kubeflow Pipelines UI.

When your pipeline function is called, each function argument will be a PipelineParam object. You can pass those objects to the components as arguments to instantiate them and create tasks. A PipelineParam can also represent an intermediate value that you pass between pipeline tasks. Each task has outputs and you can get references to them from the task.outputs[<output_name>] dictionary. The task output references can again be passed to other components as arguments.

In most cases you do not need to construct PipelineParam objects manually.

The following code sample shows how to define a pipeline with parameters:
```
@kfp.dsl.pipeline(
  name='My pipeline',
  description='My machine learning pipeline'
)
def my_pipeline(
    my_num: int = 1000, 
    my_name: str = 'some text', 
    my_url: str = 'http://example.com'
):
  ...
  # In the pipeline function body you can use the `my_num`, `my_name`, 
  # `my_url` arguments as PipelineParam objects.
```
<p align="right">(<a href="#top">back to top</a>)</p>

### Visualize Results in the Pipelines UI

#### Introduction

The Kubeflow Pipelines UI offers built-in support for several types of visualizations, which you can use to provide rich performance evaluation and comparison data. Follow the instruction below to write visualization output data to file system. You can do this at any point during the pipeline execution.

You can view the output visualizations in the following places on the Kubeflow Pipelines UI:

- The Run output tab shows the visualizations for all pipeline steps in the selected run. To open the tab in the Kubeflow Pipelines UI:

  1. Click Experiments to see your current pipeline experiments.
  2. Click the experiment name of the experiment that you want to view.
  3. Click the run name of the run that you want to view.
  4. Click the Run output tab.

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-run-output.png)


- The Visualizations tab shows the visualization for the selected pipeline step. To open the tab in the Kubeflow Pipelines UI:

  1. Click Experiments to see your current pipeline experiments.
  2. Click the experiment name of the experiment that you want to view.
  3. Click the run name of the run that you want to view.
  4. On the Graph tab, click the step representing the pipeline component that you want to view. The step details slide into
  view, showing the Visualizations tab.

![This is an image](https://www.kubeflow.org/docs/images/pipelines/confusion-matrix-task.png)

#### v2 SDK: Use SDK visualization APIs

For KFP SDK v2 and v2 compatible mode, you can use convenient SDK APIs and system artifact types for metrics visualization. Currently KFP supports ROC Curve, Confusion Matrix and Scalar Metrics formats. Full pipeline example of all metrics visualizations can be found in metrics_visualization_v2.py.

Requirements:
- Use Kubeflow Pipelines v1.7.0 or above: upgrade Kubeflow Pipelines.
- Use kfp.dsl.PipelineExecutionMode.V2_COMPATIBLE mode when compile and run your pipelines.
- Make sure to use the latest environment kustomize manifest pipelines/manifests/kustomize/env/dev/kustomization.yaml.

**Confusion Matrix**

Define Output[ClassificationMetrics] argument in your component function, then output Confusion Matrix data using API log_confusion_matrix(self, categories: List[str], matrix: List[List[int]]). categories provides a list of names for each label, matrix provides prediction performance for corresponding label. There are multiple APIs you can use for logging Confusion Matrix

```
@component(
    packages_to_install=['sklearn'],
    base_image='python:3.9'
)
def iris_sgdclassifier(test_samples_fraction: float, metrics: Output[ClassificationMetrics]):
    from sklearn import datasets, model_selection
    from sklearn.linear_model import SGDClassifier
    from sklearn.metrics import confusion_matrix

    iris_dataset = datasets.load_iris()
    train_x, test_x, train_y, test_y = model_selection.train_test_split(
        iris_dataset['data'], iris_dataset['target'], test_size=test_samples_fraction)


    classifier = SGDClassifier()
    classifier.fit(train_x, train_y)
    predictions = model_selection.cross_val_predict(classifier, train_x, train_y, cv=3)
    metrics.log_confusion_matrix(
        ['Setosa', 'Versicolour', 'Virginica'],
        confusion_matrix(train_y, predictions).tolist() # .tolist() to convert np array to list.
    )

@dsl.pipeline(
    name='metrics-visualization-pipeline')
def metrics_visualization_pipeline():
    iris_sgdclassifier_op = iris_sgdclassifier(test_samples_fraction=0.3)
```

Visualization of Confusion Matrix is as below:
![This is an image](https://www.kubeflow.org/docs/images/pipelines/v2/confusion-matrix.png)


**ROC Curve**

Define Output[ClassificationMetrics] argument in your component function, then output ROC Curve data using API log_roc_curve(self, fpr: List[float], tpr: List[float], threshold: List[float]). fpr defines a list of False Positive Rate values, tpr defines a list of True Positive Rate values, threshold indicates the level of sensitivity and specificity of this probability curve. There are multiple APIs you can use for logging ROC Curve.  
```
@component(
    packages_to_install=['sklearn'],
    base_image='python:3.9',
)
def wine_classification(metrics: Output[ClassificationMetrics]):
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.metrics import roc_curve
    from sklearn.datasets import load_wine
    from sklearn.model_selection import train_test_split, cross_val_predict

    X, y = load_wine(return_X_y=True)
    # Binary classification problem for label 1.
    y = y == 1

    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)
    rfc = RandomForestClassifier(n_estimators=10, random_state=42)
    rfc.fit(X_train, y_train)
    y_scores = cross_val_predict(rfc, X_train, y_train, cv=3, method='predict_proba')
    y_predict = cross_val_predict(rfc, X_train, y_train, cv=3, method='predict')
    fpr, tpr, thresholds = roc_curve(y_true=y_train, y_score=y_scores[:,1], pos_label=True)
    metrics.log_roc_curve(fpr, tpr, thresholds)

@dsl.pipeline(
    name='metrics-visualization-pipeline')
def metrics_visualization_pipeline():
    wine_classification_op = wine_classification()
```
![This is an image](https://www.kubeflow.org/docs/images/pipelines/v2/roc-curve.png)

**Scalar Metrics**

Define Output[Metrics] argument in your component function, then output Scalar data using API log_metric(self, metric: str, value: float). You can define any amount of metric by calling this API multiple times. metric defines the name of metric, value is the value of this metric. 

```
@component(
    packages_to_install=['sklearn'],
    base_image='python:3.9',
)
def digit_classification(metrics: Output[Metrics]):
    from sklearn import model_selection
    from sklearn.linear_model import LogisticRegression
    from sklearn import datasets
    from sklearn.metrics import accuracy_score

    # Load digits dataset
    iris = datasets.load_iris()

    # # Create feature matrix
    X = iris.data

    # Create target vector
    y = iris.target

    #test size
    test_size = 0.33

    seed = 7
    #cross-validation settings
    kfold = model_selection.KFold(n_splits=10, random_state=seed, shuffle=True)

    #Model instance
    model = LogisticRegression()
    scoring = 'accuracy'
    results = model_selection.cross_val_score(model, X, y, cv=kfold, scoring=scoring)

    #split data
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=test_size, random_state=seed)
    #fit model
    model.fit(X_train, y_train)

    #accuracy on test set
    result = model.score(X_test, y_test)
    metrics.log_metric('accuracy', (result*100.0))

@dsl.pipeline(
    name='metrics-visualization-pipeline')
def metrics_visualization_pipeline():
    digit_classification_op = digit_classification()
```

Visualization of Scalar Metrics is as below:
![This is an image](https://www.kubeflow.org/docs/images/pipelines/v2/scalar-metrics.png)


**Markdown**

Define Output[Markdown] argument in your component function, then write Markdown file to path <artifact_argument_name>.path.
```
@component
def markdown_visualization(markdown_artifact: Output[Markdown]):
    markdown_content = '## Hello world \n\n Markdown content'
    with open(markdown_artifact.path, 'w') as f:
        f.write(markdown_content)
``` 
![This is an image](https://www.kubeflow.org/docs/images/pipelines/v2/markdown-visualization.png)

**Single HTML file**

You can specify an HTML file that your component creates, and the Kubeflow Pipelines UI renders that HTML in the output page. The HTML file must be self-contained, with no references to other files in the filesystem. The HTML file can contain absolute references to files on the web. Content running inside the HTML file is sandboxed in an iframe and cannot communicate with the Kubeflow Pipelines UI.

Define Output[HTML] argument in your component function, then write HTML file to path <artifact_argument_name>.path. 
```
@component
def html_visualization(html_artifact: Output[HTML]):
    html_content = '<!DOCTYPE html><html><body><h1>Hello world</h1></body></html>'
    with open(html_artifact.path, 'w') as f:
        f.write(html_content)
```
![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-analysis-step-output-webapp-popped-out.png)

#### Source of v2 examples

The metric visualization in V2 or V2 compatible mode depends on SDK visualization APIs, refer to metrics_visualization_v2.py for a complete pipeline example. 

#### v1 SDK: Writing out metadata for the output viewers

For KFP v1, the pipeline component must write a JSON file specifying metadata for the output viewer(s) that you want to use for visualizing the results. The component must also export a file output artifact with an artifact name of mlpipeline-ui-metadata, or else the Kubeflow Pipelines UI will not render the visualization. In other words, the .outputs.artifacts setting for the generated pipeline component should show: - {name: mlpipeline-ui-metadata, path: /mlpipeline-ui-metadata.json}. The JSON filepath does not matter, although /mlpipeline-ui-metadata.json is used for consistency in the examples below.

The JSON specifies an array of outputs. Each outputs entry describes the metadata for an output viewer. The JSON structure looks like this:

```
{
  "version": 1,
  "outputs": [
    {
      "type": "confusion_matrix",
      "format": "csv",
      "source": "my-dir/my-matrix.csv",
      "schema": [
        {"name": "target", "type": "CATEGORY"},
        {"name": "predicted", "type": "CATEGORY"},
        {"name": "count", "type": "NUMBER"},
      ],
      "labels": "vocab"
    },
    {
      ...
    }
  ]
}
```
If the component writes such a file to its container filesystem, the Kubeflow Pipelines system extracts the file, and the Kubeflow Pipelines UI uses the file to generate the specified viewer(s). The metadata specifies where to load the artifact data from. The Kubeflow Pipelines UI loads the data into memory and renders it. Note: You should keep this data at a volume that’s manageable by the UI, for example by running a sampling step before exporting the file as an artifact.

The table below shows the available metadata fields that you can specify in the outputs array. Each outputs entry must have a type. Depending on value of type, other fields may also be required as described in the list of output viewers later on the page.


| Field name | Description | 
| ------------- | ------------- | 
| format	 | The format of the artifact data. The default is csv. Note: The only format currently available is csv. | 
| header	| A list of strings to be used as headers for the artifact data. For example, in a table these strings are used in the first row. |
| labpredicted_col| Name of the predicted column.| 
| schema | A list of {type, name} objects that specify the schema of the artifact data.|	
| source | The full path to the data. The available locations include http, https, Amazon S3, Minio, and Google Cloud Storage. The path can contain wildcards ‘*’, in which case the Kubeflow Pipelines UI concatenates the data from the matching source files. source can also contain inlined string data instead of a path when storage='inline'.|
| storage | (Optional) When storage is inline, the value of source is parsed as inline data instead of a path. This applies to all types of outputs except tensorboard. See Markdown or Web app below as examples. Be aware, support for inline visualizations, other than markdown, was introduced in Kubeflow Pipelines 0.2.5. Before using these visualizations, [upgrade your Kubeflow Pipelines cluster](/docs/components/pipelines/upgrade/) to version 0.2.5 or higher.|
| target_col | Name of the target column.|
| type | Name of the viewer to be used to visualize the data. The list below shows the available types.|

#### Available output viewers

The sections below describe the available viewer types and the required metadata fields for each type.

**Confusion matrix**

Type: confusion_matrix

Required metadata fields:

- format
- labels
- schema
- source
Optional metadata fields:

- storage
The confusion_matrix viewer plots a confusion matrix visualization of the data from the given source path, using the schema to  parse the data. The labels provide the names of the classes to be plotted on the x and y axes.

Specify 'storage': 'inline' to embed raw content of the confusion matrix CSV file as a string in source field directly.

Example:

```
def confusion_matrix_viz(mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
import json
  
metadata = {
  'outputs' : [{
    'type': 'confusion_matrix',
    'format': 'csv',
    'schema': [
      {'name': 'target', 'type': 'CATEGORY'},
      {'name': 'predicted', 'type': 'CATEGORY'},
      {'name': 'count', 'type': 'NUMBER'},
    ],
    'source': <CONFUSION_MATRIX_CSV_FILE>,
    # Convert vocab to string because for bealean values we want "True|False" to match csv data.
    'labels': list(map(str, vocab)),
  }]
}

with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
  json.dump(metadata, metadata_file)
```

Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-confusion-matrix-step-output.png)

**Markdown**
  
  Type: markdown

Required metadata fields:

- source
Optional metadata fields:

- storage
The markdown viewer renders Markdown strings on the Kubeflow Pipelines UI. The viewer can read the Markdown data from the following locations:

- A Markdown-formatted string embedded in the source field. The value of the storage field must be inline.
- Markdown code in a remote file, at a path specified in the source field. The storage field can be empty or contain any value except inline.
Example:
```
def markdown_vis(mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
import json
  
metadata = {
  'outputs' : [
  # Markdown that is hardcoded inline
  {
    'storage': 'inline',
    'source': '# Inline Markdown\n[A link](https://www.kubeflow.org/)',
    'type': 'markdown',
  },
  # Markdown that is read from a file
  {
    'source': 'gs://your_project/your_bucket/your_markdown_file',
    'type': 'markdown',
  }]
}

with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
  json.dump(metadata, metadata_file)
```
Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/markdown-output.png)


**ROC curve** 

Type: roc

Required metadata fields:

- format
- schema
- source
The roc viewer plots a receiver operating characteristic (ROC) curve using the data from the given source path. The Kubeflow Pipelines UI assumes that the schema includes three columns with the following names:

- fpr (false positive rate)
- tpr (true positive rate)
- thresholds
Optional metadata fields:

- storage
When viewing the ROC curve, you can hover your cursor over the ROC curve to see the threshold value used for the cursor’s closest fpr and tpr values.

Specify 'storage': 'inline' to embed raw content of the ROC curve CSV file as a string in source field directly.

Example:

```
def roc_vis(roc_csv_file_path: str, mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
  import json

  df_roc = pd.DataFrame({'fpr': fpr, 'tpr': tpr, 'thresholds': thresholds})
  roc_file = os.path.join(roc_csv_file_path, 'roc.csv')
  with file_io.FileIO(roc_file, 'w') as f:
    df_roc.to_csv(f, columns=['fpr', 'tpr', 'thresholds'], header=False, index=False)

  metadata = {
    'outputs': [{
      'type': 'roc',
      'format': 'csv',
      'schema': [
        {'name': 'fpr', 'type': 'NUMBER'},
        {'name': 'tpr', 'type': 'NUMBER'},
        {'name': 'thresholds', 'type': 'NUMBER'},
      ],
      'source': roc_file
    }]
  }

  with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
    json.dump(metadata, metadata_file)
```
Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-roc-step-output.png)


**Table**

Type: table

Required metadata fields:

- format
- header
- source
Optional metadata fields:

- storage
The table viewer builds an HTML table out of the data at the given source path, where the header field specifies the values to be shown in the first row of the table. The table supports pagination.

Specify 'storage': 'inline' to embed CSV table content string in source field directly.

Example:
```
def table_vis(mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
  import json

  metadata = {
    'outputs' : [{
      'type': 'table',
      'storage': 'gcs',
      'format': 'csv',
      'header': [x['name'] for x in schema],
      'source': prediction_results
    }]
  }

  with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
    json.dump(metadata, metadata_file)
```
Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/pipelines/taxi-tip-prediction-step-output-table.png)

**TensorBoard**

Type: tensorboard

Required metadata Fields:

- source
- The tensorboard viewer adds a Start Tensorboard button to the output page.

When viewing the output page, you can:

- Click Start Tensorboard to start a TensorBoard Pod in your Kubeflow cluster. The button text switches to Open Tensorboard.
- Click Open Tensorboard to open the TensorBoard interface in a new tab, pointing to the logdir data specified in the source field.
- Click Delete Tensorboard to shutdown the Tensorboard instance.
Note: The Kubeflow Pipelines UI doesn’t fully manage your TensorBoard instances. The “Start Tensorboard” button is a convenience feature so that you don’t have to interrupt your workflow when looking at pipeline runs. You’re responsible for recycling or deleting the TensorBoard Pods using your Kubernetes management tools.

Example:

```
def tensorboard_vis(mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
  import json

  metadata = {
    'outputs' : [{
      'type': 'tensorboard',
      'source': args.job_dir,
    }]
  }

  with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
    json.dump(metadata, metadata_file)
```
Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-training-step-output-tensorboard.png)

**Web app** 

Type: web-app

Required metadata fields:

- source
Optional metadata fields:

- storage
The web-app viewer provides flexibility for rendering custom output. You can specify an HTML file that your component creates, and the Kubeflow Pipelines UI renders that HTML in the output page. The HTML file must be self-contained, with no references to other files in the filesystem. The HTML file can contain absolute references to files on the web. Content running inside the web app is sandboxed in an iframe and cannot communicate with the Kubeflow Pipelines UI.

Specify 'storage': 'inline' to embed raw html in source field directly.

Example:
```
def tensorboard_vis(mlpipeline_ui_metadata_path: kfp.components.OutputPath()):
  import json

  static_html_path = os.path.join(output_dir, _OUTPUT_HTML_FILE)
  file_io.write_string_to_file(static_html_path, rendered_template)

  metadata = {
    'outputs' : [{
      'type': 'web-app',
      'storage': 'gcs',
      'source': static_html_path,
    }, {
      'type': 'web-app',
      'storage': 'inline',
      'source': '<h1>Hello, World!</h1>',
    }]
  }

  with open(mlpipeline_ui_metadata_path, 'w') as metadata_file:
    json.dump(metadata, metadata_file)
```
Visualization on the Kubeflow Pipelines UI:

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-analysis-step-output-webapp-popped-out.png)

#### Source of v1 examples

The v1 examples come from the tax tip prediction sample that is pre-installed when you deploy Kubeflow.

You can run the sample by selecting [Sample] ML - TFX - Taxi Tip Prediction Model Trainer from the Kubeflow Pipelines UI. For help getting started with the UI, follow the Kubeflow Pipelines quickstart.

The pipeline uses a number of prebuilt, reusable components, including:

- The Confusion Matrix component which writes out the data for the confusion_matrix viewer.
- The ROC component which writes out the data for the roc viewer.
- The dnntrainer component which writes out the data for the tensorboard viewer.
- The tfma component which writes out the data for the web-app viewer.

<p align="right">(<a href="#top">back to top</a>)</p>

### Pipeline Metrics

#### Overview of metrics

Kubeflow Pipelines supports the export of scalar metrics. You can write a list of metrics to a local file to describe the performance of the model. The pipeline agent uploads the local file as your run-time metrics. You can view the uploaded metrics as a visualization in the Runs page for a particular experiment in the Kubeflow Pipelines UI.

**Export the metrics dictionary**

To enable metrics, your component must have an output called MLPipeline Metrics and return a JSON-serialized metrics dictionary. Otherwise the Kubeflow Pipelines UI will not render the visualization. In other words, the .outputs.artifacts setting for the generated pipeline template should show: - {name: mlpipeline-metrics, path: /tmp/outputs/mlpipeline_metrics/data}. (The file path does not matter.)

An example Lightweight python component that outputs metrics dictionary by writing it to an output file:
```
from kfp.components import InputPath, OutputPath, create_component_from_func

def produce_metrics(
  # Note when the `create_component_from_func` method converts the function to a component, the function parameter "mlpipeline_metrics_path" becomes an output with name "mlpipeline_metrics" which is the correct name for metrics output.
  mlpipeline_metrics_path: OutputPath('Metrics'),
):
  import json

  accuracy = 0.9
  metrics = {
    'metrics': [{
      'name': 'accuracy-score', # The name of the metric. Visualized as the column name in the runs table.
      'numberValue':  accuracy, # The value of the metric. Must be a numeric value.
      'format': "PERCENTAGE",   # The optional format of the metric. Supported values are "RAW" (displayed in raw format) and "PERCENTAGE" (displayed in percentage format).
    }]
  }
  with open(mlpipeline_metrics_path, 'w') as f:
    json.dump(metrics, f)

produce_metrics_op = create_component_from_func(
    produce_metrics,
    base_image='python:3.7',
    packages_to_install=[],
    output_component_file='component.yaml',
)
```
Here’s an example of a lightweight Python component that outputs a metrics dictionary by returning it from the function:

```
from typing import NamedTuple
from kfp.components import InputPath, OutputPath, create_component_from_func

def produce_metrics() -> NamedTuple('Outputs', [
  ('mlpipeline_metrics', 'Metrics'),
]):
  import json

  accuracy = 0.9
  metrics = {
    'metrics': [{
      'name': 'accuracy-score', # The name of the metric. Visualized as the column name in the runs table.
      'numberValue':  accuracy, # The value of the metric. Must be a numeric value.
      'format': "PERCENTAGE",   # The optional format of the metric. Supported values are "RAW" (displayed in raw format) and "PERCENTAGE" (displayed in percentage format).
    }]
  }
  return [json.dumps(metrics)]

produce_metrics_op = create_component_from_func(
    produce_metrics,
    base_image='python:3.7',
    packages_to_install=[],
    output_component_file='component.yaml',
)
```
An example script-based component.yaml component:

```
name: Produce metrics
outputs:
- {name: MLPipeline Metrics, type: Metrics}
implementation:
  container:
    image: alpine
    command:
    - sh
    - -exc
    - |
      output_metrics_path=$0
      mkdir -p "$(dirname "$output_metrics_path")"
      echo '{
        "metrics": [{
          "name": "accuracy-score",
          "numberValue": 0.8,
          "format": "PERCENTAGE"
        }]
      }' > "$output_metrics_path"      
    - {outputPath: MLPipeline Metrics}

```

Refer to the full example of a component that generates a confusion matrix data from prediction results.

- The output name must be MLPipeline Metrics or MLPipeline_Metrics (case does not matter).

- The name of each metric must match the following pattern: ^[a-zA-Z]([-_a-zA-Z0-9]{0,62}[a-zA-Z0-9])?$.

For Kubeflow Pipelines version 0.5.1 or earlier, name must match the following pattern ^[a-z]([-a-z0-9]{0,62}[a-z0-9])?$

- numberValue must be a numeric value.

- format can only be PERCENTAGE, RAW, or not set.

**View the metrics**

To see a visualization of the metrics:

1. Open the Experiments page in the Kubeflow Pipelines UI.
2. Click one of your experiments. The Runs page opens showing the top two metrics, where top is determined by prevalence (that is, the metrics with the highest count) and then by metric name. The metrics appear as columns for each run.
The following example shows the accuracy-score and roc-auc-score metrics for two runs within an experiment:

![This is an image](https://www.kubeflow.org/docs/images/taxi-tip-run-scores.png)

<p align="right">(<a href="#top">back to top</a>)</p>

### DSL Static Type Checking

This section describes how to integrate the type information in the pipeline and utilize the static type checking for fast development iterations.

#### Motivation

A pipeline is a workflow consisting of components and each component contains inputs and outputs. The DSL compiler supports static type checking to ensure the type consistency among the component I/Os within the same pipeline. Static type checking helps you to identify component I/O inconsistencies without running the pipeline. It also shortens the development cycles by catching the errors early. This feature is especially useful in two cases:

- When the pipeline is huge and manually checking the types is infeasible;
- When some components are shared ones and the type information is not immediately available.

**Type system**

In Kubeflow pipeline, a type is defined as a type name with an OpenAPI Schema property, which defines the input parameter schema. Warning: the pipeline system currently does not check the input value against the schema when you submit a pipeline run. However, this feature will come in the near future.

There is a set of core types defined in the pipeline SDK and you can use these core types or define your custom types.

In the component YAML, types are specified as a string or a dictionary with the OpenAPI Schema, as illustrated below. “component a” expects an input with Integer type and emits three outputs with the type GCSPath, customized_type and GCRPath. Among these types, Integer, GCSPath, and GCRPath are core types that are predefined in the SDK while customized_type is a user-defined type.

```
name: component a
description: component desc
inputs:
  - {name: field_l, type: Integer}
outputs:
  - {name: field_m, type: {GCSPath: {openapi_schema_validator: {type: string, pattern: "^gs://.*$" } }}}
  - {name: field_n, type: customized_type}
  - {name: field_o, type: GCRPath} 
implementation:
  container:
    image: gcr.io/ml-pipeline/component-a
    command: [python3, /pipelines/component/src/train.py]
    args: [
      --field-l, {inputValue: field_l},
    ]
    fileOutputs: 
      field_m: /schema.txt
      field_n: /feature.txt
      field_o: /output.txt
```
Similarly, when you write a component with the decorator, you can annotate I/O with types in the function signature, as shown below.

```
from kfp.dsl import component
from kfp.dsl.types import Integer, GCRPath


@component
def task_factory_a(field_l: Integer()) -> {
    'field_m': {
        'GCSPath': {
            'openapi_schema_validator':
                '{"type": "string", "pattern": "^gs://.*$"}'
        }
    },
    'field_n': 'customized_type',
    'field_o': GCRPath()
}:
  return ContainerOp(
      name='operator a',
      image='gcr.io/ml-pipeline/component-a',
      command=['python3', '/pipelines/component/src/train.py'],
      arguments=[
          '--field-l',
          field_l,
      ],
      file_outputs={
          'field_m': '/schema.txt',
          'field_n': '/feature.txt',
          'field_o': '/output.txt'
      })
```
You can also annotate pipeline inputs with types and the input are checked against the component I/O types as well. For example,

```
@component
def task_factory_a(
    field_m: {
        'GCSPath': {
            'openapi_schema_validator':
                '{"type": "string", "pattern": "^gs://.*$"}'
        }
    }, field_o: 'Integer'):
  return ContainerOp(
      name='operator a',
      image='gcr.io/ml-pipeline/component-a',
      arguments=[
          '--field-l',
          field_m,
          '--field-o',
          field_o,
      ],
  )


# Pipeline input types are also checked against the component I/O types.
@dsl.pipeline(name='type_check', description='')
def pipeline(
    a: {
        'GCSPath': {
            'openapi_schema_validator':
                '{"type": "string", "pattern": "^gs://.*$"}'
        }
    } = 'good',
    b: Integer() = 12):
  task_factory_a(field_m=a, field_o=b)


try:
  compiler.Compiler().compile(pipeline, 'pipeline.tar.gz', type_check=True)
except InconsistentTypeException as e:
  print(e)
```

#### How does the type checking work?

The basic checking criterion is the equality checking. In other words, type checking passes only when the type name strings are equal and the corresponding OpenAPI Schema properties are equal. Examples of type checking failure are:

- “GCSPath” vs. “GCRPath”
- “Integer” vs. “Float”
- {‘GCSPath’: {‘openapi_schema_validator’: ‘{“type”: “string”, “pattern”: “^gs://.$"}'}} vs.
{‘GCSPath’: {‘openapi_schema_validator’: ‘{“type”: “string”, “pattern”: “^gcs://.$"}'}}
If inconsistent types are detected, it throws an InconsistentTypeException.

#### Type checking configuration

Type checking is enabled by default and it can be disabled in two ways:

If you compile the pipeline programmably:
```
compiler.Compiler().compile(pipeline_a, 'pipeline_a.tar.gz', type_check=False)

```
If you compile the pipeline using the dsl-compiler tool:

```
dsl-compiler --py pipeline.py --output pipeline.zip --disable-type-check
```
**Fine-grained configuration:**

- Sometimes, you might want to enable the type checking but disable certain arguments. For example, when the upstream component generates an output with type “Float” and the downstream can ingest either “Float” or “Integer”, it might fail if you define the type as “Float_or_Integer”. Disabling the type checking per-argument is also supported as shown below.
```
@dsl.pipeline(name='type_check_a', description='')
def pipeline():
  a = task_factory_a(field_l=12)
  # For each of the arguments, you can also ignore the types by calling
  # ignore_type function.
  b = task_factory_b(
      field_x=a.outputs['field_n'],
      field_y=a.outputs['field_o'],
      field_z=a.outputs['field_m'].ignore_type())

compiler.Compiler().compile(pipeline, 'pipeline.tar.gz', type_check=True)
```
**Missing types:**

- DSL compiler passes the type checking if either of the upstream or the downstream components lack the type information for some parameters. The effects are the same as that of ignoring the type information. However, type checking would still fail if some I/Os lack the type information and some I/O types are incompatible.
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Katib -->

# Katib


## Introduction to Katib
This guide introduces the concepts of hyperparameter tuning, neural architecture search, and the Katib system as a component of Kubeflow.

Katib is a Kubernetes-native project for automated machine learning (AutoML). Katib supports hyperparameter tuning, early stopping and neural architecture search (NAS). 

Katib is the project which is agnostic to machine learning (ML) frameworks. It can tune hyperparameters of applications written in any language of the users' choice and natively supports many ML frameworks, such as TensorFlow, MXNet, PyTorch, XGBoost, and others.

Katib supports a lot of various AutoML algorithms, such as Bayesian optimization, Tree of Parzen Estimators, Random Search, Covariance Matrix Adaptation Evolution Strategy, Hyperband, Efficient Neural Architecture Search, Differentiable Architecture Search and many more. Additional algorithm support is coming soon.

The Katib project is open source. The developer guide is a good starting point for developers who want to contribute to the project.

### Hyperparameters and hyperparameter tuning 
Hyperparameters are the variables that control the model training process. They include:

- The learning rate.
- The number of layers in a neural network.
- The number of nodes in each layer.
Hyperparameter values are not learned. In other words, in contrast to the node weights and other training parameters, the model training process does not adjust the hyperparameter values.

Hyperparameter tuning is the process of optimizing the hyperparameter values to maximize the predictive accuracy of the model. If you don’t use Katib or a similar system for hyperparameter tuning, you need to run many training jobs yourself, manually adjusting the hyperparameters to find the optimal values.

Automated hyperparameter tuning works by optimizing a target variable, also called the objective metric, that you specify in the configuration for the hyperparameter tuning job. A common metric is the model’s accuracy in the validation pass of the training job (validation-accuracy). You also specify whether you want the hyperparameter tuning job to maximize or minimize the metric.

For example, the following graph from Katib shows the level of validation accuracy for various combinations of hyperparameter values (the learning rate, the number of layers, and the optimizer):
![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-graph.png)
Katib runs several training jobs (known as trials) within each hyperparameter tuning job (experiment). Each trial tests a different set of hyperparameter configurations. At the end of the experiment, Katib outputs the optimized values for the hyperparameters.

You can improve your hyperparameter tunning experiments by using early stopping techniques.

### Neural architecture search 
In addition to hyperparameter tuning, Katib offers a neural architecture search feature. You can use the NAS to design your artificial neural network, with a goal of maximizing the predictive accuracy and performance of your model.

NAS is closely related to hyperparameter tuning. Both are subsets of AutoML. While hyperparameter tuning optimizes the model’s hyperparameters, a NAS system optimizes the model’s structure, node weights and hyperparameters.

NAS technology in general uses various techniques to find the optimal neural network design.

You can submit Katib jobs from the command line or from the UI. (Learn more about the Katib interfaces later on this page.) The following screenshot shows part of the form for submitting a NAS job from the Katib UI:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/nas-parameters.png)

### Katib interfaces 
You can use the following interfaces to interact with Katib:

- A web UI that you can use to submit experiments and to monitor your results. Check the getting-started guide for information on how to access the UI. The Katib home page within Kubeflow looks like this:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/home-page.png)

- A gRPC API. 

- Command-line interfaces (CLIs):

  - The Kubernetes CLI, kubectl, is useful for running commands against your Kubeflow cluster. 
- Katib Python SDK.

### Katib concepts 


This section describes the terms used in Katib.

#### Experiment
An experiment is a single tuning run, also called an optimization run.

You specify configuration settings to define the experiment. The following are the main configurations:

- **Objective:** What you want to optimize. This is the objective metric, also called the target variable. A common metric is the model’s accuracy in the validation pass of the training job (validation-accuracy). You also specify whether you want the hyperparameter tuning job to maximize or minimize the metric.

- **Search space:** The set of all possible hyperparameter values that the hyperparameter tuning job should consider for optimization, and the constraints for each hyperparameter. Other names for search space include feasible set and solution space. For example, you may provide the names of the hyperparameters that you want to optimize. For each hyperparameter, you may provide a minimum and maximum value or a list of allowable values.

- **Search algorithm:** The algorithm to use when searching for the optimal hyperparameter values.

Katib experiment is defined as a Kubernetes CRD .

#### Suggestion
A suggestion is a set of hyperparameter values that the hyperparameter tuning process has proposed. Katib creates a trial to evaluate the suggested set of values.

Katib suggestion is defined as a Kubernetes CRD .

#### Trial
A trial is one iteration of the hyperparameter tuning process. A trial corresponds to one worker job instance with a list of parameter assignments. The list of parameter assignments corresponds to a suggestion.

Each experiment runs several trials. The experiment runs the trials until it reaches either the objective or the configured maximum number of trials.

Katib trial is defined as a Kubernetes CRD .

#### Worker job
The worker job is the process that runs to evaluate a trial and calculate its objective value.

The worker job can be any type of Kubernetes resource or Kubernetes CRD. Follow the trial template guide to check how to support your own Kubernetes resource in Katib.

Katib has these CRD examples in upstream:

- Kubernetes Job

- Kubeflow TFJob

- Kubeflow PyTorchJob

- Kubeflow MXJob

- Kubeflow XGBoostJob

- Kubeflow MPIJob

- Tekton Pipelines

- Argo Workflows

By offering the above worker job types, Katib supports multiple ML frameworks.


## Getting Started with Katib

### Accessing the Katib UI 
If you installed Katib as part of Kubeflow, you can access the Katib UI from the Kubeflow UI:
![This is an image](https://www.kubeflow.org/docs/components/katib/images/home-page.png)

1. Open the Kubeflow UI. 
2. Click Katib in the left-hand menu.

### Examples 

**Example using random search algorithm**

You can create an experiment for Katib by defining the experiment in a YAML configuration file. The YAML file defines the configurations for the experiment, including the hyperparameter feasible space, optimization parameter, optimization goal, suggestion algorithm, and so on.

This example uses the YAML file for the random search example.

The random search algorithm example uses an MXNet neural network to train an image classification model using the MNIST dataset. You can check training container source code here. The experiment runs twelve training jobs with various hyperparameters and saves the results.

If you installed Katib as part of Kubeflow, you can’t run experiments in the Kubeflow namespace. Run the following commands to change namespace and launch an experiment using the random search example:

1. Download the example:
```
curl https://raw.githubusercontent.com/kubeflow/katib/master/examples/v1beta1/hp-tuning/random.yaml --output random.yaml
```

2. Edit random.yaml and change the following line to use your Kubeflow user profile namespace (e.g. kubeflow-user-example-com):
```
namespace: kubeflow
```
3. (Optional) Note: Katib’s experiments don’t work with Istio sidecar injection. If you are using Kubeflow with Istio, you have to disable sidecar injection. To do that, specify this annotation: sidecar.istio.io/inject: "false" in your experiment’s trial template.

For the provided random search example with Kubernetes Job trial template, annotation should be under .trialSpec.spec.template.metadata.annotations. For the Kubeflow TFJob or other training operators check here how to set the annotation.

4. Deploy the example:

```
kubectl apply -f random.yaml

```
This example embeds the hyperparameters as arguments. You can embed hyperparameters in another way (for example, using environment variables) by using the template defined in the trialTemplate.trialSpec section of the YAML file. The template uses the unstructured format and substitutes parameters from the trialTemplate.trialParameters. Follow the trial template guide to know more about it.

This example randomly generates the following hyperparameters:

- --lr: Learning rate. Type: double.
- --num-layers: Number of layers in the neural network. Type: integer.
- --optimizer: Optimization method to change the neural network attributes. Type: categorical.
Check the experiment status:

```
kubectl -n kubeflow-user-example-com get experiment random -o yaml
```
The output of the above command should look similar to this:

```
apiVersion: kubeflow.org/v1beta1
kind: Experiment
metadata:
  ...
  name: random
  namespace: kubeflow-user-example-com
  ...
spec:
  algorithm:
    algorithmName: random
  maxFailedTrialCount: 3
  maxTrialCount: 12
  metricsCollectorSpec:
    collector:
      kind: StdOut
  objective:
    additionalMetricNames:
      - Train-accuracy
    goal: 0.99
    metricStrategies:
      - name: Validation-accuracy
        value: max
      - name: Train-accuracy
        value: max
    objectiveMetricName: Validation-accuracy
    type: maximize
  parallelTrialCount: 3
  parameters:
    - feasibleSpace:
        max: "0.03"
        min: "0.01"
      name: lr
      parameterType: double
    - feasibleSpace:
        max: "5"
        min: "2"
      name: num-layers
      parameterType: int
    - feasibleSpace:
        list:
          - sgd
          - adam
          - ftrl
      name: optimizer
      parameterType: categorical
  resumePolicy: LongRunning
  trialTemplate:
    failureCondition: status.conditions.#(type=="Failed")#|#(status=="True")#
    primaryContainerName: training-container
    successCondition: status.conditions.#(type=="Complete")#|#(status=="True")#
    trialParameters:
      - description: Learning rate for the training model
        name: learningRate
        reference: lr
      - description: Number of training model layers
        name: numberLayers
        reference: num-layers
      - description: Training model optimizer (sdg, adam or ftrl)
        name: optimizer
        reference: optimizer
    trialSpec:
      apiVersion: batch/v1
      kind: Job
      spec:
        template:
          metadata:
            annotations:
              sidecar.istio.io/inject: "false"
          spec:
            containers:
              - command:
                  - python3
                  - /opt/mxnet-mnist/mnist.py
                  - --batch-size=64
                  - --lr=${trialParameters.learningRate}
                  - --num-layers=${trialParameters.numberLayers}
                  - --optimizer=${trialParameters.optimizer}
                image: docker.io/kubeflowkatib/mxnet-mnist:v1beta1-45c5727
                name: training-container
            restartPolicy: Never
status:
  conditions:
    - lastTransitionTime: "2021-10-07T21:12:06Z"
      lastUpdateTime: "2021-10-07T21:12:06Z"
      message: Experiment is created
      reason: ExperimentCreated
      status: "True"
      type: Created
    - lastTransitionTime: "2021-10-07T21:12:28Z"
      lastUpdateTime: "2021-10-07T21:12:28Z"
      message: Experiment is running
      reason: ExperimentRunning
      status: "True"
      type: Running
  currentOptimalTrial:
    bestTrialName: random-hpsrsdqp
    observation:
      metrics:
        - latest: "0.993054"
          max: "0.993054"
          min: "0.917694"
          name: Train-accuracy
        - latest: "0.979598"
          max: "0.979598"
          min: "0.957106"
          name: Validation-accuracy
    parameterAssignments:
      - name: lr
        value: "0.024736875661534784"
      - name: num-layers
        value: "4"
      - name: optimizer
        value: sgd
  runningTrialList:
    - random-2dwxbwcg
    - random-6jd8hmnd
    - random-7gks8bmf
  startTime: "2021-10-07T21:12:06Z"
  succeededTrialList:
    - random-xhpcrt2p
    - random-hpsrsdqp
    - random-kddxqqg9
    - random-4lkr5cjp
  trials: 7
  trialsRunning: 3
  trialsSucceeded: 4

```
When the last value in status.conditions.type is Succeeded, the experiment is complete. You can check information about the best trial in status.currentOptimalTrial.

- .currentOptimalTrial.bestTrialName is the trial name.

- .currentOptimalTrial.observation.metrics is the max, min and latest recorded values for objective and additional metrics.

- .currentOptimalTrial.parameterAssignments is the corresponding hyperparameter set.

In addition, status shows the experiment’s trials with their current status.


View the results of the experiment in the Katib UI:

1. Open the Katib UI as described above.

2. You should be able to view the list of experiments:
![This is an image](https://www.kubeflow.org/docs/components/katib/images/experiment-list.png)

3. Click the name of the experiment, random-example.

4. There should be a graph showing the level of validation and train accuracy for various combinations of the hyperparameter values (learning rate, number of layers, and optimizer):

![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-graph.png)

5. Below the graph is a list of trials that ran within the experiment:
![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-trials.png)

6. You can click on trial name to get metrics for the particular trial:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-trial-info.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Running an Experiment

*How to configure and run a hyperparameter tuning or neural architecture search experiment in Katib*

### Packaging your training code in a container image 
Katib and Kubeflow are Kubernetes-based systems. To use Katib, you must package your training code in a Docker container image and make the image available in a registry.

### Configuring the experiment 
To create a hyperparameter tuning or NAS experiment in Katib, you define the experiment in a YAML configuration file. The YAML file defines the range of potential values (the search space) for the parameters that you want to optimize, the objective metric to use when determining optimal values, the search algorithm to use during optimization, and other configurations.

As a reference, you can use the YAML file of the random search algorithm example.

The list below describes the fields in the YAML file for an experiment. The Katib UI offers the corresponding fields. You can choose to configure and run the experiment from the UI or from the command line.

#### Configuration spec 

These are the fields in the experiment configuration spec:

  - **parameters:** The range of the hyperparameters or other parameters that you want to tune for your machine learning (ML
  model. The parameters define the search space, also known as the feasible set or the solution space. In this section of the
  spec, you define the name and the distribution (discrete or continuous) of every hyperparameter that you need to search. For
  example, you may provide a minimum and maximum value or a list of allowed values for each hyperparameter. Katib generates
  hyperparameter combinations in the range based on the hyperparameter tuning algorithm that you specify. Refer to the
  ParameterSpec type.

  - **objective:** The metric that you want to optimize. The objective metric is also called the target variable. A common metric
  is the model’s accuracy in the validation pass of the training job (validation-accuracy). You also specify whether you want
  Katib to maximize or minimize the metric.

  Katib uses the objectiveMetricName and additionalMetricNames to monitor how the hyperparameters work with the model. Katib
  records the value of the best objectiveMetricName metric (maximized or minimized based on type) and the corresponding
  hyperparameter set in the experiment’s .status.currentOptimalTrial.parameterAssignments. If the objectiveMetricName metric for
  a set of hyperparameters reaches the goal, Katib stops trying more hyperparameter combinations.

  You can run the experiment without specifying the goal. In that case, Katib runs the experiment until the corresponding
  successful trials reach maxTrialCount. maxTrialCount parameter is described below.

  The default way to calculate the experiment’s objective is:

  - When the objective type is maximize, Katib compares all maximum metric values.

  - When the objective type is minimize, Katib compares all minimum metric values.

  To change the default settings, define metricStrategies with various rules (min, max or latest) to extract values for each
  metric from the experiment’s objectiveMetricName and additionalMetricNames. The experiment’s objective value is calculated in
  accordance with the selected strategy.

  For example, you can set the parameters in your experiment as follows:

    ```
    . . .
    objectiveMetricName: accuracy
    type: maximize
    metricStrategies:
      - name: accuracy
        value: latest
    . . .

    ```
  where the Katib controller is searching for the best maximum from the all latest reported accuracy metrics for each trial.
  Check the metrics strategies example. The default strategy type for each metric is equal to the objective type.

  Refer to the ObjectiveSpec type.

  - **parallelTrialCount:** The maximum number of hyperparameter sets that Katib should train in parallel. The default value is 3.

  - **maxTrialCount:** The maximum number of trials to run. This is equivalent to the number of hyperparameter sets that Katib should generate to test the model. If the maxTrialCount value is omitted, your experiment will be running until the objective goal is reached or the experiment reaches a maximum number of failed trials.

  - **maxFailedTrialCount:** The maximum number of failed trials before Katib should stop the experiment. This is equivalent to the number of failed hyperparameter sets that Katib should test. If the number of failed trials exceeds maxFailedTrialCount, Katib stops the experiment with a status of Failed.

  - **algorithm:** The search algorithm that you want Katib to use to find the best hyperparameters or neural architecture configuration. Examples include random search, grid search, Bayesian optimization, and more. Check the search algorithm details below.

  - **trialTemplate:** The template that defines the trial. You have to package your ML training code into a Docker image, as described above. trialTemplate.trialSpec is your unstructured template with model parameters, which are substituted from trialTemplate.trialParameters. For example, your training container can receive hyperparameters as command-line arguments or as environment variables. You have to set the name of your training container in trialTemplate.primaryContainerName.

  Katib dynamically supports any kind of Kubernetes CRD. In Katib examples, you can find the following job types to train your model:

    - Kubernetes Job

    - Kubeflow TFJob

    - Kubeflow PyTorchJob

    - Kubeflow MXJob

    - Kubeflow XGBoostJob

    - Kubeflow MPIJob

    - Tekton Pipelines

    - Argo Workflows

  Refer to the TrialTemplate type. Follow the trial template guide to understand how to specify trialTemplate parameters, save templates in ConfigMaps and support custom Kubernetes resources in Katib.

  - metricsCollectorSpec: A specification of how to collect the metrics from each trial, such as the accuracy and loss metrics. Learn the details of the metrics collector below. The default metrics collector is StdOut.

  - nasConfig: The configuration for a neural architecture search (NAS). Note: NAS is currently in alpha with limited support. You can specify the configurations of the neural network design that you want to optimize, including the number of layers in the network, the types of operations, and more. Refer to the NasConfig type.

  - graphConfig: The graph config that defines structure for a directed acyclic graph of the neural network. You can specify the number of layers, input_sizes for the input layer and output_sizes for the output layer. Refer to the GraphConfig type.

  - operations: The range of operations that you want to tune for your ML model. For each neural network layer the NAS algorithm selects one of the operations to build a neural network. Each operation contains sets of parameters which are described above. Refer to the Operation type.

  You can find all NAS examples here.

  - resumePolicy: The experiment resume policy. Can be one of LongRunning, Never or FromVolume. The default value is LongRunning. Refer to the ResumePolicy type. To find out how to modify a running experiment and use various restart policies follow the resume an experiment guide.

  Background information about Katib’s Experiment, Suggestion and Trial type: In Kubernetes terminology, Katib’s Experiment type, Suggestion type and Trial type is a custom resource (CR). The YAML file that you create for your experiment is the CR specification.

#### Search algorithms in detail 
Katib currently supports several search algorithms. Refer to the AlgorithmSpec type.

Here’s a list of the search algorithms available in Katib:

- Grid search
- Random search
- Bayesian optimization
- Hyperband
- Tree of Parzen Estimators (TPE)
- Multivariate TPE
- Covariance Matrix Adaptation Evolution Strategy (CMA-ES)
- Sobol’s Quasirandom Sequence
- Neural Architecture Search based on ENAS
- Differentiable Architecture Search (DARTS)

**Grid search**

The algorithm name in Katib is grid.

Grid sampling is useful when all variables are discrete (as opposed to continuous) and the number of possibilities is low. A grid search performs an exhaustive combinatorial search over all possibilities, making the search process extremely long even for medium sized problems.

Katib uses the Chocolate optimization framework for its grid search.


**Random search**

The algorithm name in Katib is random.

Random sampling is an alternative to grid search and is used when the number of discrete variables to optimize is large and the time required for each evaluation is long. When all parameters are discrete, random search performs sampling without replacement. Random search is therefore the best algorithm to use when combinatorial exploration is not possible. If the number of continuous variables is high, you should use quasi random sampling instead.

Katib uses the Hyperopt, Goptuna, Chocolate or Optuna optimization framework for its random search.

Katib supports the following algorithm settings:

| Setting name | Description | Example |
| ------------- | ------------- | ------------- | 
|random_state |	[int]: Set random_state to something other than None for reproducible results. |	10


**Bayesian optimization**

The algorithm name in Katib is bayesianoptimization.

The Bayesian optimization method uses Gaussian process regression to model the search space. This technique calculates an estimate of the loss function and the uncertainty of that estimate at every point in the search space. The method is suitable when the number of dimensions in the search space is low. Since the method models both the expected loss and the uncertainty, the search algorithm converges in a few steps, making it a good choice when the time to complete the evaluation of a parameter configuration is long.

Katib uses the Scikit-Optimize or Chocolate optimization framework for its Bayesian search. Scikit-Optimize is also known as skopt.

Katib supports the following algorithm settings:

| Setting name | Description | Example |
| ------------- | ------------- | ------------- | 
|base_estimator|[“GP”, “RF”, “ET”, “GBRT” or sklearn regressor, default=“GP”]: Should inherit from sklearn.base.RegressorMixin. The predict method should have an optional return_std argument, which returns std(Y | x) along with E[Y | x]. If base_estimator is one of [“GP”, “RF”, “ET”, “GBRT”], the system uses a default surrogate model of the corresponding type. |	GP |
| n_initial_points |[int,  default=10]: Number of evaluations of func with initialization points before approximating it with base_estimator. Points provided as x0 count as initialization points. If len(x0) < n_initial_points, the system samples additional points at random. |	10|
| acq_func |	[string, default="gp_hedge"]: The function to minimize over the posterior distribution. | gp_hedge |
| acq_optimizer |	[string, “sampling” or “lbfgs”, default=“auto”]: The method to minimize the acquisition function. The system updates the fit model with the optimal value obtained by optimizing acq_func with acq_optimizer. |	auto|
| random_state |	[int]: Set random_state to something other than None for reproducible results. |	10 |


**Hyperband**

The algorithm name in Katib is hyperband.

Katib supports the Hyperband optimization framework. Instead of using Bayesian optimization to select configurations, Hyperband focuses on early stopping as a strategy for optimizing resource allocation and thus for maximizing the number of configurations that it can evaluate. Hyperband also focuses on the speed of the search.


**Tree of Parzen Estimators (TPE)**
The algorithm name in Katib is tpe.

Katib uses the Hyperopt, Goptuna or Optuna optimization framework for its TPE search.

This method provides a forward and reverse gradient-based search.

Katib supports the following algorithm settings:

| Setting name | Description | Example |
| ------------- | ------------- | ------------- | 
| n_EI_candidates	| [int]: Number of candidate samples used to calculate the expected improvement. |	25|
| random_state |	[int]: Set random_state to something other than None for reproducible results. |	10|
| gamma |	[float]: The threshold to split between l(x) and g(x), check equation 2 in this Paper. Value must be in (0, 1) range.	|0.25|
|prior_weight |	[float]: Smoothing factor for counts, to avoid having 0 probability. Value must be > 0.|	1.1|

**Multivariate TPE**

The algorithm name in Katib is multivariate-tpe.

Katib uses the Optuna optimization framework for its Multivariate TPE search.

Multivariate TPE is improved version of independent (default) TPE. This method finds dependencies among hyperparameters in search space.

Katib supports the following algorithm settings:

| Setting name | Description | Example |
| ------------- | ------------- | ------------- | 
| n_ei_candidates |	[int]: Number of Trials used to calculate the expected improvement. |	25| 
| random_state | 	[int]: Set random_state to something other than None for reproducible results.	| 10| 
| n_startup_trials	| [int]: Number of initial Trials for which the random search algorithm generates hyperparameters.	| 5| 


**Covariance Matrix Adaptation Evolution Strategy (CMA-ES)**
The algorithm name in Katib is cmaes.

Katib uses the Goptuna or Optuna optimization framework for its CMA-ES search.

The Covariance Matrix Adaptation Evolution Strategy is a stochastic derivative-free numerical optimization algorithm for optimization problems in continuous search spaces. You can also use IPOP-CMA-ES and BIPOP-CMA-ES, variant algorithms for restarting optimization when converges to local minimum.

Katib supports the following algorithm settings:

| Setting name | Description | Example |
| ------------- | ------------- | ------------- | 
|random_state	|[int]: Set random_state to something other than None for reproducible results.|	10|
|sigma|	[float]: Initial standard deviation of CMA-ES.|	0.001|
|restart_strategy|	[string, "none", "ipop", or "bipop", default="none"]: Strategy for restarting CMA-ES optimization when converges to a local minimum.|	"ipop"|

**Sobol’s Quasirandom Sequence**
The algorithm name in Katib is sobol.

Katib uses the Goptuna optimization framework for its Sobol’s quasirandom search.

The Sobol’s quasirandom sequence is a low-discrepancy sequence. And it is known that Sobol’s quasirandom sequence can provide better uniformity properties.

**Neural Architecture Search based on ENAS**

The algorithm name in Katib is enas.

This NAS algorithm is ENAS-based. Currently, it doesn’t support parameter sharing.

Katib supports the following algorithm settings:

| Setting Name| 	Type| 	Default value	| Description| 
| ------------- | ------------- | ------------- | ------------- | 
| controller_hidden_size| 	int| 	64| 	RL controller lstm hidden size. Value must be >= 1.| 
| controller_temperature| 	float| 	5.0| 	RL controller temperature for the sampling logits. Value must be > 0. Set value to "None" to disable it in the controller.| 
| controller_tanh_const| 	float| 	2.25| 	RL controller tanh constant to prevent premature convergence. Value must be > 0. Set value to "None" to disable it in the controller.| 
| controller_entropy_weight| 	float| 	1e-5| 	RL controller weight for entropy applying to reward. Value must be > 0. Set value to "None" to disable it in the controller.| 
| controller_baseline_decay| 	float	| 0.999	| RL controller baseline factor. Value must be > 0 and <= 1.| 
| controller_learning_rate | 	float| 	5e-5| 	RL controller learning rate for Adam optimizer. Value must be > 0 and <= 1.| 
| controller_skip_target| 	float| 	0.4| 	RL controller probability, which represents the prior belief of a skip connection being formed. Value must be > 0 and <= 1.| 
| controller_skip_weight| 	float| 	0.8| 	RL controller weight of skip penalty loss. Value must be > 0. Set value to "None" to disable it in the controller.| 
| controller_train_steps| 	int| 	50| 	Number of RL controller training steps after each candidate runs. Value must be >= 1.| 
| controller_log_every_steps| 	int	| 10| 	Number of RL controller training steps before logging it. Value must be >= 1.| 

**Differentiable Architecture Search (DARTS)**

The algorithm name in Katib is darts.
| Setting Name| 	Type| 	Default value	| Description| 
| ------------- | ------------- | ------------- | ------------- | 
|num_epochs|	int	|50	|Number of epochs to train model|
|w_lr|	float	|0.025	|Initial learning rate for training model weights. This learning rate annealed down to w_lr_min following a cosine schedule without restart.|
|w_lr_min|	float	|0.001	|Minimum learning rate for training model weights.|
|w_momentum|	float|	0.9	|Momentum for training training model weights.|
|w_weight_decay|	float|	3e-4|	Training model weight decay.|
|w_grad_clip|	float |5.0|	Max norm value for clipping gradient norm of training model weights.|
|alpha_lr	|float|	3e-4	|Initial learning rate for alphas weights.|
|alpha_weight_decay |	float|	1e-3|	Alphas weight decay.|
|batch_size	|int	|128|	Batch size for dataset.|
|num_workers|	int|	4	|Number of subprocesses to download the dataset.|
|init_channels|	int	|16	|Initial number of channels.|
|print_step	|int|	50	|Number of training or validation steps before logging it.|
|num_nodes|	int|	4	|Number of DARTS nodes.|
|stem_multiplier|	int|	3	|Multiplier for initial channels. It is used in the first stem cell.|

**Metrics collector**

In the metricsCollectorSpec section of the YAML configuration file, you can define how Katib should collect the metrics from each trial, such as the accuracy and loss metrics. Refer to the MetricsCollectorSpec type

Your training code can record the metrics into stdout or into arbitrary output files. Katib collects the metrics using a sidecar container. A sidecar is a utility container that supports the main container in the Kubernetes Pod.

To define the metrics collector for your experiment:

1.Specify the collector type in the .collector.kind field. Katib’s metrics collector supports the following collector types:

  - StdOut: Katib collects the metrics from the operating system’s default output location (standard output). This is the default metrics collector.

  - File: Katib collects the metrics from an arbitrary file, which you specify in the .source.fileSystemPath.path field. Training container should log metrics to this file. Check the file metrics collector example. The default file path is /var/log/katib/metrics.log.

  - TensorFlowEvent: Katib collects the metrics from a directory path containing a tf.Event. You should specify the path in the .source.fileSystemPath.path field. Check the TFJob example. The default directory path is /var/log/katib/tfevent/.

  - Custom: Specify this value if you need to use a custom way to collect metrics. You must define your custom metrics collector container in the .collector.customCollector field. Check the custom metrics collector example.

  - None: Specify this value if you don’t need to use Katib’s metrics collector. For example, your training code may handle the persistent storage of its own metrics.

2. Write code in your training container to print or save to the file metrics in the format specified in the .source.filter.metricsFormat field. The default format is ([\w|-]+)\s*=\s*([+-]?\d*(\.\d+)?([Ee][+-]?\d+)?). Each element is a regular expression with two subexpressions. The first matched expression is taken as the metric name. The second matched expression is taken as the metric value.

For example, using the default metrics format and StdOut metrics collector, if the name of your objective metric is loss and the additional metrics are recall and precision, your training code should print the following output:
```
epoch 1:
loss=3.0e-02
recall=0.5
precision=.4

epoch 2:
loss=1.3e-02
recall=0.55
precision=.5
```

### Running the experiment 
To run a hyperparameter tuning experiment from the Katib UI:

1. Follow the getting-started guide to access the Katib UI.

2. Click NEW EXPERIMENT on the Katib home page.

3. You should be able to view tabs offering you the following options:

  - YAML file: Choose this option to supply an entire YAML file containing the configuration for the experiment.

  ![This is an image](https://www.kubeflow.org/docs/components/katib/images/deploy-yaml.png)

  - Parameters: Choose this option to enter the configuration values into a form.

  ![This is an image](https://www.kubeflow.org/docs/components/katib/experiment/)

View the results of the experiment in the Katib UI:

1. You should be able to view the list of experiments:
  
![This is an image](https://www.kubeflow.org/docs/components/katib/images/experiment-list.png)

2. Click the name of your experiment. For example, click random-example.

3. There should be a graph showing the level of validation and train accuracy for various combinations of the hyperparameter values (learning rate, number of layers, and optimizer):

![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-graph.png)

4. Below the graph is a list of trials that ran within the experiment:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-trials.png)

5. You can click on trial name to get metrics for the particular trial:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/random-example-trial-info.png)

<p align="right">(<a href="#top">back to top</a>)</p>

## Resuming an Experiment
*How to restart and modify running experiments*

### Modify running experiment 

While the experiment is running you are able to change trial count parameters. For example, you can decrease the maximum number of hyperparameter sets that are trained in parallel.

You can change only parallelTrialCount, maxTrialCount and maxFailedTrialCount experiment parameters.

Use Kubernetes API or kubectl in-place update of resources to make experiment changes. For example, run:

```
kubectl edit experiment <experiment-name> -n <experiment-namespace>

```
Make appropriate changes and save it. Controller automatically processes the new parameters and makes necessary changes.

- If you want to increase or decrease parallel trial execution, modify parallelTrialCount. Controller accordingly creates or deletes trials in line with the parallelTrialCount value.

- If you want to increase or decrease maximum trial count, modify maxTrialCount. maxTrialCount should be greater than current count of Succeeded trials. You can remove the maxTrialCount parameter, if your experiment should run endless with parallelTrialCount of parallel trials until the experiment reaches Goal or maxFailedTrialCount

- If you want to increase or decrease maximum failed trial count, modify maxFailedTrialCount. You can remove the maxFailedTrialCount parameter, if the experiment should not reach Failed status.

### Resume succeeded experiment 

Katib experiment is restartable only if it is in Succeeded status because maxTrialCount has been reached. To check current experiment status run: kubectl get experiment <experiment-name> -n <experiment-namespace>.

To restart an experiment, you are able to change only parallelTrialCount, maxTrialCount and maxFailedTrialCount as described above

To control various resume policies, you can specify .spec.resumePolicy for the experiment. Refer to the ResumePolicy type.

#### Resume policy: Never
Use this policy if your experiment should not be resumed at any time. After the experiment has finished, the suggestion’s Deployment and Service are deleted and you can’t restart the experiment. Learn more about Katib concepts in the overview guide.

Check the never-resume.yaml example for more details.

#### Resume policy: LongRunning
Use this policy if you intend to restart the experiment. After the experiment has finished, the suggestion’s Deployment and Service stay running. Modify experiment’s trial count parameters to restart the experiment.

When you delete the experiment, the suggestion’s Deployment and Service are deleted.

This is the default policy for all Katib experiments. You can omit .spec.resumePolicy parameter for that functionality.

#### Resume policy: FromVolume
Use this policy if you intend to restart the experiment. In that case, volume is attached to the suggestion’s Deployment.

Katib controller creates PersistentVolumeClaim (PVC) in addition to the suggestion’s Deployment and Service.

Note: Your Kubernetes cluster must have StorageClass for dynamic volume provisioning to automatically provision storage for the created PVC. Otherwise, you have to define suggestion’s PersistentVolume (PV) specification in the Katib configuration settings and Katib controller will create PVC and PV. Follow the Katib configuration guide to set up the suggestion’s volume settings.

- PVC is deployed with the name: <suggestion-name>-<suggestion-algorithm> in the suggestion namespace.

- PV is deployed with the name: <suggestion-name>-<suggestion-algorithm>-<suggestion-namespace>

After the experiment has finished, the suggestion’s Deployment and Service are deleted. Suggestion data can be retained in the volume. When you restart the experiment, the suggestion’s Deployment and Service are created and suggestion statistics can be recovered from the volume.

When you delete the experiment, the suggestion’s Deployment, Service, PVC and PV are deleted automatically.

Check the from-volume-resume.yaml example for more details.

<p align="right">(<a href="#top">back to top</a>)</p>

## Overview of Trial Templates
*How to specify trial template parameters and support a custom resource (CRD) in Katib*

Katib has these CRD examples in upstream:

- Kubernetes Job

- Kubeflow TFJob

- Kubeflow PyTorchJob

- Kubeflow MXJob

- Kubeflow XGBoostJob

- Kubeflow MPIJob

- Tekton Pipelines

- Argo Workflows

To use your own Kubernetes resource follow the steps below.

For the details on how to configure and run your experiment, follow the running an experiment guide.

### Use trial template to submit experiment
To run the Katib experiment you have to specify a trial template for your worker job where actual training is running. Learn more about Katib concepts in the overview guide.

#### Configure trial template specification
Trial template specification is located under .spec.trialTemplate of your experiment. For the API overview refer to the TrialTemplate type.

To define experiment’s trial, you should specify these parameters in .spec.trialTemplate:

- trialParameters - list of the parameters which are used in the trial template during experiment execution.

  Note: Your trial template must contain each parameter from the trialParameters. You can set these parameters in any field of your template, except .metadata.name and .metadata.namespace. Check below how you can use trial metadata parameters in your template. For example, your training container can receive hyperparameters as command-line or arguments or as environment variables.

  Your experiment’s suggestion produces trialParameters before running the trial. Each trialParameter has these structure:

  - name - the parameter name that is replaced in your template.

  - description (optional) - the description of the parameter.

  - reference - the parameter name that experiment’s suggestion returns. Usually, for the hyperparameter tuning parameter references are equal to the experiment search space. For example, in grid example search space has three parameters (lr, num-layers and optimizer) and trialParameters contains each of these parameters in reference.

- You have to define your experiment’s trial template in one of the trialSpec or configMap sources.

  Note: Your template must omit .metadata.name and .metadata.namespace.

  To set the parameters from the trialParameters, you need to use this expression: ${trialParameters.<parameter-name>} in your template. Katib automatically replaces it with the appropriate values from the experiment’s suggestion.

  For example, --lr=${trialParameters.learningRate} is the learningRate parameter.

  - trialSpec - the experiment’s trial template in unstructured format. The template should be a valid YAML. Check the grid example.

  - configMap - Kubernetes ConfigMap specification where the experiment’s trial template is located. This ConfigMap must have the label katib.kubeflow.org/component: trial-templates and contains key-value pairs, where key: <template-name>, value: <template-yaml>. Check the example of the ConfigMap with trial templates.

    The configMap specification should have:

    1. configMapName - the ConfigMap name with the trial templates.

    2. configMapNamespace - the ConfigMap namespace with the trial templates.

    3. templatePath - the ConfigMap’s data path to the template.

  Check the example with ConfigMap source for the trial template.

.spec.trialTemplate parameters below are used to control trial behavior. If parameter has the default value, it can be omitted in the experiment YAML.

  - retain - indicates that trial’s resources are not clean-up after the trial is complete. Check the example with retain: true parameter.

  The default value is false

  - primaryPodLabels - the trial worker’s Pod or Pods labels. These Pods are injected by Katib metrics collector.

  Note: If primaryPodLabels is omitted, the metrics collector wraps all worker’s Pods. Learn more about Katib metrics collector in running an experiment guide. Check the example with primaryPodLabels.

  The default value for Kubeflow TFJob, PyTorchJob, MXJob, and XGBoostJob is job-role: master

  The primaryPodLabels default value works only if you specify your template in .spec.trialTemplate.trialSpec. For the configMap template source you have to manually set primaryPodLabels.

  - primaryContainerName - the training container name where actual model training is running. Katib metrics collector wraps this container to collect required metrics for the single experiment optimization step.

  - successCondition - The trial worker’s object status in which trial’s job has succeeded. This condition must be in GJSON format. Check the example with successCondition.

  The default value for Kubernetes Job is status.conditions.#(type=="Complete")#|#(status=="True")#

  The default value for Kubeflow TFJob, PyTorchJob, MXJob, and XGBoostJob is status.conditions.#(type=="Succeeded")#|#(status=="True")#

  The successCondition default value works only if you specify your template in .spec.trialTemplate.trialSpec. For the configMap template source you have to manually set successCondition.

  - failureCondition - The trial worker’s object status in which trial’s job has failed. This condition must be in GJSON format. Check the example with failureCondition.

  The default value for Kubernetes Job is status.conditions.#(type=="Failed")#|#(status=="True")#

  The default value for Kubeflow TFJob, PyTorchJob, MXJob, and XGBoostJob is status.conditions.#(type=="Failed")#|#(status=="True")#

  The failureCondition default value works only if you specify your template in .spec.trialTemplate.trialSpec. For the configMap template source you have to manually set failureCondition.


#### Use trial metadata in template
You can’t specify .metadata.name and .metadata.namespace in your trial template, but you can get this data during the experiment run. For example, if you want to append the trial’s name to your model storage.

To do this, point .trialParameters[x].reference to the appropriate metadata parameter and use .trialParameters[x].name in your trial template.

The table below shows the connection between .trialParameters[x].reference value and trial metadata.

You can’t specify .metadata.name and .metadata.namespace in your trial template, but you can get this data during the experiment run. For example, if you want to append the trial’s name to your model storage.

To do this, point .trialParameters[x].reference to the appropriate metadata parameter and use .trialParameters[x].name in your trial template.

The table below shows the connection between .trialParameters[x].reference value and trial metadata.


| Reference	| 	Trial metadata| 
| ------------- | ------------- | 
| ${trialSpec.Name}| 	Trial name| 
| ${trialSpec.Namespace}| 	Trial namespace| 
| ${trialSpec.Kind}	| Kubernetes resource kind for the trial's worker| 
| ${trialSpec.APIVersion}	| Kubernetes resource APIVersion for the trial's worker| 
| ${trialSpec.Labels[custom-key]}	| Trial's worker label with custom-key key| 
| ${trialSpec.Annotations[custom-key]}	| Trial's worker annotation with custom-key key| 

### Use custom Kubernetes resource as a trial template

In Katib examples you can find the following trial worker types: Kubernetes Job, Kubeflow TFJob, Kubeflow PyTorchJob, Kubeflow MXJob, Kubeflow XGBoostJob, Kubeflow MPIJob, Tekton Pipelines, and Argo Workflows.

It is possible to use your own Kubernetes CRD or other Kubernetes resource (e.g. Kubernetes Deployment) as a trial worker without modifying Katib controller source code and building the new image. As long as your CRD creates Kubernetes Pods, allows to inject the sidecar container on these Pods and has succeeded and failed status, you can use it in Katib.

To do that, you need to modify Katib components before installing it on your Kubernetes cluster. Accordingly, you have to know your CRD API group and version, the CRD object’s kind. Also, you need to know which resources your custom object is created. 

Follow these two simple steps to integrate your custom CRD in Katib:

  1. Modify Katib controller ClusterRole’s rules with the new rule to give Katib access to all resources that are created by the trial. To know more about ClusterRole, check Kubernetes guide.

  In case of Tekton Pipelines, trial creates Tekton PipelineRun, then Tekton PipelineRun creates Tekton TaskRun. Therefore, Katib controller ClusterRole should have access to the pipelineruns and taskruns:
  ```
  - apiGroups:
    - tekton.dev
  resources:
    - pipelineruns
    - taskruns
  verbs:
    - "*"
  ```
  2. Modify Katib controller Deployment’s args with the new flag: --trial-resources=<object-kind>.<object-API-version>.<object-API-group>.

  For example, to support Tekton Pipelines:
  ```
  - "--trial-resources=PipelineRun.v1beta1.tekton.dev"

  ```

After these changes, deploy Katib as described in the getting started guide and wait until the katib-controller Pod is created. You can check logs from the Katib controller to verify your resource integration:

```
$ kubectl logs $(kubectl get pods -n kubeflow -o name | grep katib-controller) -n kubeflow | grep '"CRD Kind":"PipelineRun"'

{"level":"info","ts":1628032648.6285546,"logger":"trial-controller","msg":"Job watch added successfully","CRD Group":"tekton.dev","CRD Version":"v1beta1","CRD Kind":"PipelineRun"}

```

If you ran the above steps successfully, you should be able to use your custom object YAML in the experiment’s trial template source spec.
<p align="right">(<a href="#top">back to top</a>)</p>

## Using Early Stopping

*How to use early stopping in Katib experiments*

Early stopping allows you to avoid overfitting when you train your model during Katib experiments. It also helps by saving computing resources and reducing experiment execution time by stopping the experiment’s trials when the target metric(s) no longer improves before the training process is complete.

The major advantage of using early stopping in Katib is that you don’t need to modify your training container package. All you have to do is make necessary changes in your experiment’s YAML file.

Early stopping works in the same way as Katib’s metrics collector. It analyses required metrics from the stdout or from the arbitrary output file and an early stopping algorithm makes the decision if the trial needs to be stopped. Currently, early stopping works only with StdOut or File metrics collectors.

Note: Your training container must print training logs with the timestamp, because early stopping algorithms need to know the sequence of reported metrics. Check the MXNet example to learn how to add a date format to your logs.

### Configure the experiment with early stopping 
As a reference, you can use the YAML file of the early stopping example.

1. Follow the guide to configure your Katib experiment.

2. Next, to apply early stopping for your experiment, specify the .spec.earlyStopping parameter, similar to the .spec.algorithm. Refer to the EarlyStoppingSpec type for more information.

  - .earlyStopping.algorithmName - the name of the early stopping algorithm.

  - .earlyStopping.algorithmSettings- the settings for the early stopping algorithm.

What happens is your experiment’s suggestion produces new trials. After that, the early stopping algorithm generates early stopping rules for the created trials. Once the trial reaches all the rules, it is stopped and the trial status is changed to the EarlyStopped. Then, Katib calls the suggestion again to ask for the new trials.

Follow the Katib configuration guide to specify your own image for the early stopping algorithm.

#### Early stopping algorithms in detail
Here’s a list of the early stopping algorithms available in Katib:

- Median Stopping Rule
More algorithms are under development.

You can add an early stopping algorithm to Katib yourself. Check the developer guide to contribute.


**Median Stopping Rule**

The early stopping algorithm name in Katib is medianstop.

The median stopping rule stops a pending trial X at step S if the trial’s best objective value by step S is worse than the median value of the running averages of all completed trials' objectives reported up to step S.

To learn more about it, check Google Vizier: A Service for Black-Box Optimization.

Katib supports the following early stopping settings:
| Setting name | Description |  Default value |
| ------------- | ------------- | ------------- |
|min_trials_required|	Minimal number of successful trials to compute median value|	3|
|start_step|	Number of reported intermediate results before stopping the trial|	4|

#### Submit an early stopping experiment from the UI 

You can use Katib UI to submit an early stopping experiment. Follow these steps to create an experiment from the UI.

Once you reach the early stopping section, select the appropriate values:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/early-stopping-parameter.png)

### View the early stopping experiment results 
First, make sure you have jq installed.

Check the early stopped trials in your experiment:

```
kubectl get experiment <experiment-name>  -n <experiment-namespace> -o json | jq -r ".status"

```

The last part of the above command output looks similar to this:

```
 . . .
  "earlyStoppedTrialList": [
    "median-stop-2ml8h96d",
    "median-stop-cgjkq8zn",
    "median-stop-pvn5p54p",
    "median-stop-sjc9tcgc"
  ],
  "startTime": "2020-11-05T03:03:43Z",
  "succeededTrialList": [
    "median-stop-2kmh57qf",
    "median-stop-7ccstz4z",
    "median-stop-7sqt7556",
    "median-stop-lgvhfch2",
    "median-stop-mkfjtwbj",
    "median-stop-nfmgqd7w",
    "median-stop-nsbxw5m9",
    "median-stop-nsmhg4p2",
    "median-stop-rp88xflk",
    "median-stop-xl7dlf5n",
    "median-stop-ztc58kwq"
  ],
  "trials": 15,
  "trialsEarlyStopped": 4,
  "trialsSucceeded": 11
}
```

Check the status of the early stopped trial by running this command:

```
kubectl get trial median-stop-2ml8h96d -n <experiment-namespace>

```
and you should be able to view EarlyStopped status for the trial:

```
NAME                   TYPE           STATUS   AGE
median-stop-2ml8h96d   EarlyStopped   True     15m

```
In addition, you can check your results on the Katib UI. The trial statuses on the experiment monitor page should look as follows:
![This is an image](https://www.kubeflow.org/docs/components/katib/images/early-stopping-trials.png)

You can click on the early stopped trial name to get reported metrics before this trial is early stopped:

![This is an image](https://www.kubeflow.org/docs/components/katib/images/early-stopping-trial-info.png)




